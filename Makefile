OCAMLBUILD_FLAGS = -use-ocamlfind -r -pkgs menhirLib,core,oUnit -tags "optimize(3)"
OCAMLBUILD_MENHIRFLAGS = -use-menhir -menhir "menhir --explain"
#OCAMLBUILD_MENHIRFLAGS = -use-menhir -menhir "menhir --explain --trace"

# Suppress duplicate topdirs.cmi warnings.
OCAMLFIND_IGNORE_DUPS_IN = $(shell ocamlfind query compiler-libs)
export OCAMLFIND_IGNORE_DUPS_IN


all: main.native
profile: main.p.native
byte: main.byte

main.native main.p.native main.byte:
	ocamlbuild $(OCAMLBUILD_MENHIRFLAGS) $(OCAMLBUILD_FLAGS) $@

clean:
	ocamlbuild -clean

.PHONY: main.native main.byte main.p.native

