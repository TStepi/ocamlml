open Utils

(* Getting data from arff or srff files *)

let read_arff name =
  let channel = In_channel.create name in
  let lexbuf = Lexing.from_channel channel in
  try
    Arffparser.file Arfflexer.token lexbuf
  with
  | Arffparser.Error -> Myerror.syntax ~loc:(Mylocation.of_lexeme lexbuf) "I am confused"
  | Failure(lexing_empty_token) -> Myerror.syntax ~loc:(Mylocation.of_lexeme lexbuf) "Unknown character"


(* Processes arff header and data *)
let process_arff {Arff.relation=r; Arff.attributes=attrs} data filename =

  let get_structs attrs=
    let convert_attribute attr = match attr with
      | (name, Arff.Real) ->
        Some (Structure.Real)
      | (name, Arff.Integer) ->
        Some (Structure.Integer)
      | (name, Arff.Nominal lst) ->
        if List.length lst = 2 then
          Some (Structure.Binary)
        else
          Some (Structure.Nominal (List.to_array lst))
      | (name, Arff.String) ->
        None
      | (name, Arff.Date format) ->
        None
    in
    List.filter_map ~f:convert_attribute attrs
  in

  let process_attr (str, attr) =
    if str="?" then Some Data.missing else
      try
        match attr with
        | (name, Arff.Real) ->
          Some (Data.real (Float.of_string str))
        | (name, Arff.Integer) ->
          Some (Data.integer (Int.of_string str))
        | (name, Arff.Nominal lst) ->
          Some (Data.nominal (index_of_exn str (List.to_array lst)))
        | (name, Arff.String) ->
          None
        | (name, Arff.Date format) ->
          None
      with
      | _ -> printf "Prebran niz %s ne ustreza atributu %s\n" str (Arff.attribute_repr attr); failwith "error"
  in

  let process_line line =
    let zipped = List.map2_exn ~f:(fun x y -> (x,y)) line attrs in
    List.filter_map ~f:process_attr zipped |> List.to_array
  in

  let names = List.map attrs ~f:fst |> List.to_array in
  let structs = get_structs attrs |> List.to_array in
  let datalist = List.map ~f:process_line data
                 |> List.to_array in
  {
    Data.name = r;
    Data.structures = structs;
    Data.values = datalist;
    Data.variable_names = names;
    Data.size = Array.length datalist
  }



(* let read_srff name = *)
(*   let channel = open_in name in *)
(*   let lexbuf = Lexing.from_channel channel in *)
(*   try *)
(*     Srffparser.file Srfflexer.token lexbuf *)
(*   with *)
(*   | Srffparser.Error -> Myerror.syntax ~loc:(Mylocation.of_lexeme lexbuf) "I am confused" *)
(*   | Failure(lexing_empty_token) -> Myerror.syntax ~loc:(Mylocation.of_lexeme lexbuf) "Unknown character" *)


(* (\* Processes srff header and data *\) *)
(* let process_srff {Srff.name=r; Srff.types=ts; Srff.input=a; Srff.output=c} data = *)

(*   let rec construct_struct type_list str = *)
(*     let t = List.Assoc.find type_list str in *)
(*     let t = match t with Some x -> x | None -> failwith (sprintf "Cannot find %s" str) in *)
(*     match t with *)
(*     | Srff.Real -> Structure.Real (0.,0.,0.) *)
(*     | Srff.Integer -> Structure.Integer (0.,0.,0.) *)
(*     | Srff.Nominal lst -> Structure.Nominal (List.to_array lst,[||]) *)
(*     (\*| Srff.String -> Structure.Sequence (Structure.Nominal [])*\) *)
(*     | Srff.String -> failwith "Nizev še ne delam" *)
(*     | Srff.Date format -> failwith "Datumov še ne delam" *)
(*     | Srff.Set s -> Structure.Set (construct_struct type_list s) *)
(*     | Srff.Sequence s -> Structure.Sequence (construct_struct type_list s) *)
(*     | Srff.Tuple lst -> Structure.Tuple (Array.init (List.length lst) (fun i -> 1.), List.to_array @@ List.map ~f:(construct_struct type_list) lst) *)
(*     | Srff.Series (num,s) -> Structure.Series (Int.of_string num, construct_struct type_list s) *)
(*   in *)

(*   let rec process_data structure data = *)
(*     match structure,data with *)
(*     | _, Srff.Item "?" -> Data.missing *)
(*     | Structure.Real _, Srff.Item s -> Data.real (Float.of_string s) *)
(*     | Structure.Integer _, Srff.Item s -> Data.integer (Int.of_string s) *)
(*     | Structure.Nominal (lst,_), Srff.Item s -> Data.nominal (index_of_exn s lst) *)
(*     | Structure.Tuple (ws,lst1), Srff.Tp lst2 -> Data.tuple (let lst2 = List.to_array lst2 in Array.map2_exn ~f:process_data lst1 lst2 ) *)
(*     | Structure.Set s, Srff.St lst -> Data.set (List.to_array @@ List.map ~f:(process_data s) lst) *)
(*     | Structure.Sequence s, Srff.Sq lst -> Data.sequence (List.to_array @@ List.map ~f:(process_data s) lst) *)
(*     (\*| Structure.Sequence (Structure.Nominal []), Srff.Item s -> Data.Sequence (List.to_array @@ List.map ~f:(fun x -> Data.Nominal (Char.to_string x)) (String.to_list s))*\) *)
(*     | Structure.Sequence _, Srff.Item s -> failwith "Nizev še ne delam" *)
(*     | Structure.Series (n,s), Srff.Se lst -> *)
(*       if List.length lst <> n then failwith "Wrong series length" else *)
(*       Data.series (List.to_array @@ List.map lst ~f:(process_data s)) *)
(*     | _,_ -> *)
(*       printf "Cannot proccess following structure-data pair:\n"; *)
(*       printf "%s\n" (Structure.to_string structure); *)
(*       printf "%s\n" (Srff.datum_repr data); *)
(*       failwith "cannot process srff data" *)
(*   in *)

(*   let astruct = construct_struct ts a in *)
(*   let cstruct = construct_struct ts c in *)
(*   let inputs = List.map data ~f:(fun (x,_) -> process_data astruct x) |> List.to_array in *)
(*   let outputs = List.map data ~f:(fun (_,y) -> process_data cstruct y) |> List.to_array in *)
(*   { *)
(*     Data.name = r; *)
(*     Data.input_structure = Data.update_structure astruct inputs; *)
(*     Data.output_structure = Data.update_structure cstruct outputs; *)
(*     Data.size = List.length data; *)
(*     Data.inputs = inputs; *)
(*     Data.outputs = outputs *)
(*   } *)


let load name =
  let extension = String.sub name (String.length name - 5) 5 in
  if extension = ".arff" then
    let (header, data) = read_arff name in
    process_arff header data name
    (* else if extension = ".srff" then *)
    (* let (header, data) = read_srff name in *)
    (* process_srff header data *)
  else
    failwith (sprintf "Uknown file extension %s" extension)
