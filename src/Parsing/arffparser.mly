%{

%}

%token ATTRIBUTE DATA RELATION
%token REAL NUMERIC INTEGER STRING DATE
%token LBRACE RBRACE
%token COMMA NEWLINE
%token <string> WORD
%token EOF

%start file
%type <Arff.header * Arff.row list> file

%%

file:
  | RELATION relname=WORD NEWLINE
    attrs=list(attribute)
    DATA NEWLINE
    rows=list(row)
    EOF
    { ({Arff.relation=relname; Arff.attributes=attrs}, rows) }
  | NEWLINE
    RELATION relname=WORD NEWLINE
    attrs=list(attribute)
    DATA NEWLINE
    rows=list(row)
    EOF
    { ({Arff.relation=relname; Arff.attributes=attrs}, rows) }

attribute:
  | ATTRIBUTE name=WORD ty=datatype NEWLINE { (name, ty) }

datatype:
  | NUMERIC                                         { Arff.Real }
  | REAL                                            { Arff.Real }
  | INTEGER                                         { Arff.Integer }
  | STRING                                          { Arff.String }
  | DATE d=datespec                                 { Arff.Date d }
  | LBRACE lst=separated_list(COMMA, choice) RBRACE { Arff.Nominal lst }

datespec:
  |                 { None }
  | s=WORD          { Some s }

choice:
  | c=WORD          { c }

row:
  | lst=separated_list(COMMA, WORD) NEWLINE { lst }
