{
  open Arffparser

  let declarations = [
    ("attribute", ATTRIBUTE);
    ("data", DATA);
    ("relation", RELATION)
  ] ;;

  let keywords = [
    ("numeric", NUMERIC);
    ("real", REAL);
    ("integer", INTEGER);
    ("string", STRING);
    ("date", DATE);
  ] ;;

  let escaped_characters = [
    ("\"", "\"");
    ("\\", "\\");
    ("\'", "'");
    ("n", "\n");
    ("t", "\t");
    ("b", "\b");
    ("r", "\r");
    (" ", " ");
  ]
}

let declaration = '@' ['a'-'z' 'A'-'Z']+

let word = ['a'-'z' 'A'-'Z' '0'-'9' '-' '+' ':' '/' '_' '[' ']' '$' '%' '^' '!' '?' '@' '#' '*' '=' '.']+

let white = [' ' '\t' '\r' '\n']*

rule token = parse
  | white '\n'          { Lexing.new_line lexbuf; NEWLINE }
  | [' ' '\r' '\t']     { token lexbuf }
  | '%' [^ '\n']* white '\n' { Lexing.new_line lexbuf; token lexbuf }
  | declaration                 { let s = Lexing.lexeme lexbuf in
                                  let s = String.lowercase_ascii (String.sub s 1 (String.length s - 1)) in
                                  try
                                    List.assoc s declarations
                                  with Not_found ->
                                    Myerror.syntax ~loc:(Mylocation.of_lexeme lexbuf) "Unknown declaration %s"
                                                (Lexing.lexeme lexbuf)
                                }
  | '{'                         { LBRACE }
  | '}'                         { RBRACE }
  | ','                         { COMMA }
  | '"'                         { WORD (string_double "" lexbuf) }
  | '\''                        { WORD (string_single "" lexbuf) }
  | word                        { let s = Lexing.lexeme lexbuf in
                                  try
                                    List.assoc (String.lowercase_ascii s) keywords
                                  with Not_found -> WORD s }
  | eof                         { EOF }

and string_double acc = parse
  | '"'                 { acc }
  | '\\'                { let esc = escaped lexbuf in string_double (acc ^ esc) lexbuf }
  | [^'"' '\\']*        { string_double (acc ^ (Lexing.lexeme lexbuf)) lexbuf }
  | eof                 { Myerror.syntax ~loc:(Mylocation.of_lexeme lexbuf) "Unterminated string"}

and string_single acc = parse
  | '\''                { acc }
  | '\\'                { let esc = escaped lexbuf in string_single (acc ^ esc) lexbuf }
  | [^'\'' '\\']*       { string_single (acc ^ (Lexing.lexeme lexbuf)) lexbuf }
  | eof                 { Myerror.syntax ~loc:(Mylocation.of_lexeme lexbuf) "Unterminated string %s" acc}

and escaped = parse
  | _                   { let str = Lexing.lexeme lexbuf in
                          try List.assoc str escaped_characters
                          with Not_found ->
                            Myerror.syntax ~loc:(Mylocation.of_lexeme lexbuf) "Unknown escaped character %s" str
                        }

{
}
