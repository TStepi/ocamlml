%{

%}

%token TYPE DATA NAME INPUT OUTPUT
%token REAL INTEGER STRING DATE SET TUPLE SEQUENCE NOMINAL SERIES
%token LBRACKET RBRACKET (* LSQUARE RSQUARE LBRACE RBRACE *)
%token COMMA NEWLINE EQUALS
%token <string> WORD
%token EOF

%start file
%type <Srff.header * (Srff.data * Srff.data) list> file

%%

file:
  | NAME name=WORD NEWLINE
    types=list(types)
    INPUT EQUALS input=WORD NEWLINE
    OUTPUT EQUALS output=WORD NEWLINE
    DATA NEWLINE
    rows=list(row)
    EOF
    {(
        {
          Srff.name=name;
          Srff.types=types;
          Srff.input=input;
          Srff.output=output
        },
        rows
    )}
  | NEWLINE
    NAME name=WORD NEWLINE
    types=list(types)
    INPUT EQUALS input=WORD NEWLINE
    OUTPUT EQUALS output=WORD NEWLINE
    DATA NEWLINE
    rows=list(row)
    EOF
    {(
        {
          Srff.name=name;
          Srff.types=types;
          Srff.input=input;
          Srff.output=output
        },
        rows
    )}

types:
  | TYPE name=WORD EQUALS ty=datatype NEWLINE { (name, ty) }

datatype:
  | REAL                                                          { Srff.Real }
  | INTEGER                                                       { Srff.Integer }
  | STRING                                                        { Srff.String }
  | DATE d=datespec                                               { Srff.Date d }
  | TUPLE LBRACKET lst=separated_list(COMMA, WORD) RBRACKET       { Srff.Tuple lst }
  | SERIES LBRACKET c=WORD COMMA d=WORD RBRACKET                               { Srff.Series (c,d) }
  | SET LBRACKET c=WORD RBRACKET                                  { Srff.Set c }
  | SEQUENCE LBRACKET c=WORD RBRACKET                             { Srff.Sequence c }
  | NOMINAL LBRACKET lst=separated_list(COMMA, WORD) RBRACKET     { Srff.Nominal lst }

datespec:
  |                 { None }
  | s=WORD          { Some s }

row:
  | a=data COMMA c=data NEWLINE { (a,c) }

data:
  | TUPLE LBRACKET lst=separated_list(COMMA, data) RBRACKET       { Srff.Tp lst }
  | SET LBRACKET lst=separated_list(COMMA, data) RBRACKET         { Srff.St lst }
  | SEQUENCE LBRACKET lst=separated_list(COMMA, data) RBRACKET    { Srff.Sq lst }
  | SERIES LBRACKET lst=separated_list(COMMA, data) RBRACKET      { Srff.Se lst }
  | c=WORD                                                        { Srff.Item c }
