open Utils

type date_format = string

type datatype =
    | Real
    | Integer
    | Nominal of string list
    | String
    | Date of date_format option
    | Set of string
    | Tuple of string list
    | Sequence of string
    | Series of string*string

type data =
    | Tp of data list
    | St of data list
    | Sq of data list
    | Se of data list
    | Item of string

type header = {
    name : string ;
    types : (string * datatype) list;
    input : string;
    output : string
}

let header_repr {name=r; types=ts; input=a; output=c} =
  let datatype_repr (name, ty) =
    let type_repr = match ty with
    | Real -> sprintf "real"
    | Integer -> sprintf "integer"
    | Nominal lst -> sprintf "Nominal(%s)" (String.concat ~sep:"," lst)
    | String -> sprintf "string"
    | Date None -> sprintf "date"
    | Date (Some d) -> sprintf "date \"%s\"" d
    | Set name -> sprintf "set(%s)" name
    | Tuple lst -> sprintf "tuple(%s)" (String.concat ~sep:"," lst)
    | Sequence name -> sprintf "sequence(%s)" name
    | Series (len,name) -> sprintf "series(%s,%s)" len name
    in
    sprintf "@type %s %s" name type_repr
  in
  sprintf "@RELATION %s\n%s@ATTRIBUTE %s\n@CLASS %s\n" r (String.concat ~sep:"\n" (List.map ~f:datatype_repr ts)) a c

let rec datum_repr data =
    match data with
    | Tp lst -> sprintf "Tuple( %s )" (String.concat ~sep:", " (List.map ~f:datum_repr lst))
    | St lst -> sprintf "Set( %s )" (String.concat ~sep:", " (List.map ~f:datum_repr lst))
    | Sq lst -> sprintf "Sequence( %s )" (String.concat ~sep:", " (List.map ~f:datum_repr lst))
    | Item s -> sprintf "%s" s
    | Se lst -> sprintf "Series( %s )" (String.concat ~sep:", " (List.map ~f:datum_repr lst))

let data_repr rows=
  Printf.sprintf "@DATA\n%s\n" (String.concat ~sep:"\n" (List.map ~f:datum_repr rows))
