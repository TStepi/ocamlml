(** Data structures for the ARFF format,
    see http://www.cs.waikato.ac.nz/ml/weka/arff.html *)

open Utils

type date_format = string

type datatype =
    | Real
    | Integer
    | Nominal of string list
    | String
    | Date of date_format option

type header = {
    relation : string ;
    attributes : (string * datatype) list
}

type column = string
type row = column list

let attribute_repr (name, ty) =
  let datatype_repr ty = match ty with
  | Real -> sprintf "real"
  | Integer -> sprintf "integer"
  | Nominal lst -> sprintf "{%s}" (String.concat ~sep:"," lst)
  | String -> sprintf "string"
  | Date None -> sprintf "date"
  | Date (Some d) -> sprintf "date \"%s\"" d
  in
  sprintf "@ATTRIBUTE %s %s" name (datatype_repr ty)

let header_repr {relation=r; attributes=attrs} =
  sprintf "@RELATION %s\n%s\n" r (String.concat ~sep:"\n" (List.map attrs ~f:attribute_repr))

let data_repr rows =
    sprintf "@DATA\n%s\n" (String.concat ~sep:"\n" (List.map rows ~f:(fun x -> String.concat ~sep:", " x)))
