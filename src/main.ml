open Utils
module S = Settings

(* Main loop *)
let () =
  let start = Time.now () in
  Unittesting.run ();
  print_endline "\n";

  (* let settings = S.read_settings Sys.argv.(1) in *)
  let settings = S.read_settings "settings.oc" in
  let dataset = Reader.load settings.S.dataset_path in
  (* let dataset = Datatransform.binary_tuple_to_set dataset 0 1023 in *)
  let root_subset = Data.create_root_subset dataset settings.S.disabled settings.S.targets in
  let root_subset =
    if settings.S.franks then
      {root_subset with Data.importances = S.get_ranks settings.S.dataset_path}
    else root_subset
  in

  sprintf "Dataset has %d attributes" (Array.length dataset.Data.structures)
  |> print_endline;
  sprintf "We designated %s as targets"
    (array_to_string ~f:Int.to_string root_subset.Data.targets)
  |> print_endline;

  (*Transform dataset to distance features.*)
  (* let module DG = Distancegenerator in *)
  (* let distance_generator = DG.construct_pool *)
  (*     ~tuples:[|DG.Average |] *)
  (*     ~series:[|DG.WarpingS 100; DG.AverageS|] *)
  (*     ~sequences:[|DG.Warping 100|] *)
  (*     ~sets:[|DG.Hausdorff; DG.Jaccard|] in *)

  (* let new_dataset = Distancefeatures.transform_dataset *)
  (*     ~subset:root_subset *)
  (*     ~distance_generator *)
  (*     ~rounds:10 *)
  (*     ~subspace:0.2 *)
  (*     ~anchors:50 in *)

  (* let new_targets = Array.init (Array.length S.targets) ~f:(fun i -> *)
  (*     Array.length new_dataset.Data.structures - i - 1) in *)
  (* let root_subset = Data.create_root_subset new_dataset [||] new_targets in *)
  (* Data.subset_to_arff root_subset "myChembl.arff"; *)


  (*Performance evaluation*)
  let out_stats = Statistic.subset_target_stats root_subset in
  let out_structure = Data.target_structure_of_subset root_subset in
  let out_distance = S.output_distance_generator out_stats in

  let opt_measure = S.optimisation_measure out_structure out_stats in
  let rep_measures = S.report_measures settings.S.threshold out_structure out_stats in

  Reports.init ~report_path:settings.S.report_path ~data:root_subset ~comments:S.comments;

  let task = {
    Task.data = root_subset;
    Task.target_distance = out_distance;
    Task.main_measure = opt_measure;
  } in

  let results = Evaluation.evaluate (S.validation settings) task S.algorithm_list in

  Array.iter results ~f:(fun r ->
      Reports.model_report
        ~show_model:false
        ~report_path:settings.S.report_path
        ~measures:rep_measures.(0)
        ~result:r
    );

  let stop = Time.now () in
  print_endline (sprintf "Finished in: %.3f" (Core.Time.Span.to_sec @@ Time.diff stop start))
