open Utils

let basic:
  k:int ->
  distance_generator:Distancegenerator.t ->
  description:string ->
  (module Algorithm.Modeler) =
  fun ~k ~distance_generator ~description ->
    (module struct

      let log () = ""
      let description = description

      let predict train_attributes train_targets centroid distance sample =
        let candidates = Array.mapi train_attributes ~f:(fun i x ->
            let d = distance sample x in
            (train_targets.(i), (1. -. d) /. (1. +. d))) in
        Array.permute candidates;
        partialsort candidates k (fun (d1,w1) (d2,w2) -> Float.descending w1 w2);
        Array.map (Array.sub candidates 0 k)
          ~f:(fun (d,w) -> {d with Data.reliability = w})
        |> centroid


      let induce task =
        let start_time = time () in
        let trainset = task.Task.data in
        let centroid = Aggregation.calculate
            task.Task.target_distance
            (Statistic.subset_target_stats trainset)
        in
        let train_attributes = Data.get_attributes trainset in
        let train_targets = Data.get_targets trainset in
        let distance = Statistic.subset_attribute_stats trainset
                       |> distance_generator
                       |> Distance.interpret
        in
        let induction_time = time () -. start_time in
        [|{
          Model.description = description;
          Model.to_string = (fun () -> "I'm just a poor kNN");
          Model.predict = predict train_attributes train_targets centroid distance;
        },induction_time |]

    end : Algorithm.Modeler)
