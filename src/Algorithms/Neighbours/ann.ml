open Utils

let construct ~distance_generator ~description =
(module struct
  let train_set = ref {Data.indices = [||]; Data.inputs = [||]; Data.outputs = [||]}
  let distance_pool = ref [||]
  let calculator = ref (fun _ _ -> 1.)
  let chosen_distance = ref Distance.Delta
  let success_measure = ref Measure.relative_misses
  let centroid = ref (Centroid.calculate Distance.Delta (Structure.Real (0.,0.,0.)))
  let centroid_multi = ref (Centroid.calculate_multi Distance.Delta (Structure.Real (0.,0.,0.)))

  let logger = ref [] 
  let log () = String.concat ~sep:"\n" (List.rev !logger)
  let description = description

  let weight d = 1. /. d -. 1.

  let predict' train ignore dist sample =
    let candidates = Array.map train.Data.indices ~f:(fun i ->
      if i=ignore then (Data.missing, Float.neg_infinity)
      else (train.Data.outputs.(i), weight (dist sample train.Data.inputs.(i)))
    ) in
    let data,weights = Array.unzip candidates in
    (*float_array_translate weights (float_array_min weights);*)
    !centroid weights data

  let predict data =
    predict' !train_set (-1) !calculator data


  let choose_d struc trainset =
    let solutions = Array.map trainset.Data.indices ~f:(fun i -> trainset.Data.outputs.(i)) in
    let best_perf = ref 2. in
    let best_dist = ref Distance.Delta in

    for i=0 to Array.length !distance_pool do
      let dist = !distance_pool.(i) in
      let predictions = Array.map trainset.Data.indices ~f:(fun i ->
        predict' trainset i (Distance.interpret dist) trainset.Data.inputs.(i)
      ) in
      let perf = !success_measure.Measure.calculate solutions predictions in
      if perf < !best_perf then
        (best_perf := perf; best_dist := dist)
    done;
    chosen_distance := !best_dist;
    calculator := Distance.interpret !best_dist;
    logger := (sprintf "Chose %s" (Distance.to_string ~short:true !best_dist)) :: !logger


  let learn ~input_structure ~output_structure ~output_distance_generator ~measure_selector ~trainset =
    let output_distance = output_distance_generator output_structure in
    distance_pool := distance_generator input_structure;
    chosen_distance := !distance_pool.(0);
    success_measure := measure_selector output_structure output_distance_generator;
    centroid := Centroid.calculate output_distance output_structure;
    centroid_multi := Centroid.calculate_multi output_distance output_structure;
    train_set := trainset;
    if (Array.length !distance_pool > 1) then
      choose_d input_structure trainset
    else
      chosen_distance := Distance.update input_structure !chosen_distance;
      calculator := Distance.interpret !chosen_distance

end : Algorithm.Predictor)