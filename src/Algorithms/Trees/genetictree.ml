open Utils
module D = Data
module NS = Nodestats

type t = {
  mutable attribute : int;
  mutable index : int;
  mutable left : t option;
  mutable right : t option
}

type solution = {
  repr : t;
  tree : Tree.t;
  fitness : float
}


let create_test_pool subset =
  Array.map subset.D.attributes ~f:(fun a ->
      match subset.D.dataset.D.structures.(a) with
      | Structure.Binary ->
        [|{
          Test.run = (fun x -> if D.is_missing x then None else
                         match x.D.components.(a).D.value with
                         | Some 0. -> Some false
                         | Some 1. -> Some true
                         | _ -> None
                     );
          Test.description = sprintf "%s = 1" (subset.D.dataset.D.variable_names.(a))
        }|]

      | Structure.Nominal names ->
        Array.mapi names ~f:(fun i n ->{
              Test.run = (fun x -> if D.is_missing x then None else
                             match x.D.components.(a).D.value with
                             | None -> None
                             | Some v -> Some (v = (float i))
                         );
              Test.description = sprintf "%s = %s"
                  (subset.D.dataset.D.variable_names.(a))
                  names.(i)
            })

      | (Structure.Real | Structure.Integer) ->
        let values = Array.filter_map subset.D.samples ~f:(fun i ->
            subset.D.dataset.D.values.(i).(a).D.value
          ) in
        Array.sort values ~cmp:Float.ascending;
        let tests = ref [] in
        let i= ref 1 in
        let last_value = ref values.(0) in
        while !i < Array.length values do
          if values.(!i) = !last_value then incr i else begin
            let split_point = (!last_value +. values.(!i))/. 2. in
            last_value := values.(!i);
            tests := {
              Test.run = (fun x -> if D.is_missing x then None else
                             match x.D.components.(a).D.value with
                             | None -> None
                             | Some v -> Some (v > split_point)
                         );
              Test.description = sprintf "%s > %.3f"
                  (subset.D.dataset.D.variable_names.(a)) split_point
            } :: !tests
          end
        done;
        Array.of_list !tests

      | _ -> failwith "Not doing splits over this type"
    )

let rec copy_repr repr =
  {
    attribute = repr.attribute;
    index = repr.index;
    left = Option.map repr.left copy_repr;
    right = Option.map repr.right copy_repr;
  }


let random_node repr =
  let seen = ref 0 in
  let chosen = ref {attribute = 0; index = 0; left = None; right = None} in

  let rec iter lst =
    match lst with
    | [] -> ()
    | None :: rest -> iter rest
    | (Some t) :: rest ->
      incr seen;
      if Random.int !seen = !seen - 1 then chosen := t;
      iter (t.left :: t.right :: rest)
  in
  iter [Some repr];
  !chosen

let random_test pool =
  let attribute = ref (Random.int (Array.length pool)) in
  while Array.length pool.(!attribute) < 1 do
    attribute := Random.int (Array.length pool)
  done;
  let index = Random.int (Array.length pool.(!attribute)) in
  !attribute,index

let random_index pool attribute =
  Random.int (Array.length pool.(attribute))


let rec random_repr pool depth =
  let (a,i) = random_test pool in
  {
    attribute = a;
    index = i;
    left = if depth=1 then None else Some (random_repr pool (depth-1));
    right = if depth=1 then None else Some (random_repr pool (depth-1))
  }


let mutate pool repr =
  let t = random_node repr in
  let choice = Random.int 3 in
  if choice = 0 then begin
    match t.left with
    | Some _ ->
      t.left <- None;
      t.right <- None
    | None ->
      let (a,i) = random_test pool in
      t.left <- Some {attribute = a; index = i; left = None; right = None};
      let (a,i) = random_test pool in
      t.right <- Some {attribute = a; index = i; left = None; right = None}
  end else if choice = 1 then begin
    let (a,i) = random_test pool in
    t.attribute <- a;
    t.index <- i;
  end else
    t.index <- random_index pool t.attribute


let crossover pool repr1 repr2 =
  let basis = copy_repr repr1 in
  let t1 = random_node basis in
  let t2 = random_node repr2 in

  t1.attribute <- t2.attribute;
  t1.index <- t2.index;
  t1.left <- t2.left;
  t1.right <- t2.right;
  basis

let rec tree_of_repr parameters pool repr subset stats =

  let left_stats = {
    NS.size = 0.;
    NS.impurity = Float.infinity;
    NS.target_stats = Array.map stats.NS.target_stats ~f:Statistic.empty_copy;
    NS.attribute_stats = Array.map stats.NS.attribute_stats ~f:Statistic.empty_copy;
  } in
  let right_stats = {
    NS.size = 0.;
    NS.impurity = Float.infinity;
    NS.target_stats = Array.map stats.NS.target_stats ~f:Statistic.empty_copy;
    NS.attribute_stats = Array.map stats.NS.attribute_stats ~f:Statistic.empty_copy;
  } in
  let left_samples = ref [] in
  let right_samples = ref [] in

  let test = pool.(repr.attribute).(repr.index) in

  for i=0 to Array.length subset.D.samples -1 do
    match test.Test.run (D.get_attribute subset subset.D.samples.(i)) with
    | Some true ->
      NS.add_example right_stats subset i parameters;
      right_samples := subset.D.samples.(i) :: !right_samples
    | Some false ->
      NS.add_example left_stats subset i parameters;
      left_samples := subset.D.samples.(i) :: !left_samples
    | None ->
      NS.add_example left_stats subset i parameters;
      NS.add_example right_stats subset i parameters;
      right_samples := subset.D.samples.(i) :: !right_samples;
      left_samples := subset.D.samples.(i) :: !left_samples
  done;

  let left_subset = {subset with D.samples = List.to_array !left_samples} in
  let right_subset = {subset with D.samples = List.to_array !right_samples} in


  if Float.equal (Float.min right_stats.NS.size left_stats.NS.size) 0. then begin
    let proto = parameters.Treeparameters.aggregate (Data.get_targets subset) in
    let r = 1. -. stats.Nodestats.impurity in
    Tree.Leaf {
      Tree.leaf_stats = stats;
      Tree.prototype = {proto with Data.reliability = r}
    }
  end else begin
    NS.update_impurity parameters right_stats;
    NS.update_impurity parameters left_stats;
    Tree.Split {
      Tree.test = test;
      Tree.split_stats = stats;

      Tree.left = (match repr.left with
          | None ->
            let proto = parameters.Treeparameters.aggregate (Data.get_targets left_subset) in
            let r = 1. -. left_stats.NS.impurity in
            Tree.Leaf {
              Tree.leaf_stats = left_stats;
              Tree.prototype = {proto with Data.reliability = r}
            }
          | Some t -> tree_of_repr parameters pool t left_subset left_stats);

      Tree.right = (match repr.right with
          | None ->
            let proto = parameters.Treeparameters.aggregate (Data.get_targets right_subset) in
            let r = 1. -. right_stats.NS.impurity in
            Tree.Leaf {
              Tree.leaf_stats = right_stats;
              Tree.prototype = {proto with Data.reliability = r}
            }
          | Some t -> tree_of_repr parameters pool t right_subset right_stats);
    }
  end


let evaluate_tree example_num tree size_matters =
  let average_impurity = ref 0. in
  let total_size = ref 0. in
  let leaves = ref 0 in

  let rec iter lst =
    match lst with
    | [] -> ()
    | (Tree.Leaf l) :: rest ->
      incr leaves;
      total_size := !total_size +. l.Tree.leaf_stats.NS.size;
      average_impurity := !average_impurity +.
                          l.Tree.leaf_stats.NS.size *.
                          l.Tree.leaf_stats.NS.impurity;
      iter rest
    | (Tree.Split s) :: rest ->
      iter (s.Tree.left :: s.Tree.right :: rest)
  in
  iter [tree];

  average_impurity := !average_impurity /. !total_size;
  let average_size = !total_size /. ((float !leaves) *. example_num) in
  size_matters *. (1. -. average_size) +. (1. -. size_matters) *. !average_impurity


let initial_member pool depth subset starting_stats parameters size_matters =
  let repr = random_repr pool depth in
  let tree = tree_of_repr parameters pool repr subset starting_stats in
  {
    repr;
    tree;
    fitness = evaluate_tree (Array.length subset.D.samples |> float)
        tree size_matters
  }


let evolve ~supervision ~size_matters ~generations ~interval ~population_size ~description =
  (module struct
    let log () = ""
    let description = description

    let induce task =
      let trainset = task.Task.data in
      let total_samples = Array.length trainset.D.samples |> float in
      let initial_depth = Array.length trainset.D.samples
                          |> Float.of_int |> Float.log |> (fun x -> Int.of_float x-3)
                          |> Int.max 1 in

      let aggregate = Aggregation.calculate
          task.Task.target_distance
          (Statistic.subset_target_stats trainset)
      in

      let pool = create_test_pool trainset in
      let starting_stats = NS.of_subset trainset in
      let parameters = {
        Treeparameters.supervision = supervision;
        Treeparameters.minimum_node_size = 2;
        Treeparameters.aggregate = aggregate;
        Treeparameters.attribute_normalization =
          Test.get_normalizations (Statistic.subset_attribute_stats trainset);
        Treeparameters.target_normalization =
          Test.get_normalizations (Statistic.subset_target_stats trainset);
        Treeparameters.importances = trainset.Data.importances;
        Treeparameters.subspaces = All
      } in

      let population = ref (Array.init population_size ~f:(fun _ ->
          initial_member pool initial_depth trainset starting_stats parameters size_matters
        )) in

      let results = ref [] in
      let start_time = time () in

      for i=1 to generations do
        let offsprings = Array.init population_size ~f:(fun _ ->
            let p1 = !population.(Random.int population_size) in
            let p2 = !population.(Random.int population_size) in
            let new_repr = crossover pool p1.repr p2.repr in
            mutate pool new_repr;
            let new_tree = tree_of_repr parameters pool new_repr trainset starting_stats in
            let new_fit = evaluate_tree total_samples new_tree size_matters in
            {repr = new_repr; tree = new_tree; fitness = new_fit}
          ) in
        let candidates = Array.append !population offsprings in
        Array.sort candidates ~cmp:(fun a b -> Float.ascending a.fitness b.fitness);
        population := Array.sub candidates 0 population_size;

        if i=generations || i mod interval = 0 then begin
          print_endline @@ array_to_string ~f:(fun x -> Float.to_string x.fitness) !population;
          let current_time = time () -. start_time in
          let current_trees = Array.mapi !population ~f:(fun j m -> {
                Model.description = sprintf "%d-th genetic PCT after %d iterations" j i;
                Model.to_string = (fun () -> Tree.to_string m.tree);
                Model.predict = (fun x -> Tree.predict m.tree x aggregate)
              }) in
          let current_ensemble = Model.make_ensemble
                                   (sprintf "Genetic ensemble after %d iterations" i)
                                   current_trees aggregate in
            results := (current_ensemble,current_time) :: (current_trees.(0),current_time) :: !results;
        end
      done;
      Array.of_list_rev !results

  end : Algorithm.Modeler)
