type t = {
  supervision : float;
  attribute_normalization : float array;
  target_normalization : float array;
  minimum_node_size : int;
  importances : float array;
  subspaces : Utils.subarray_size;
  aggregate : Aggregation.t
}
