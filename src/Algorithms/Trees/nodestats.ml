open Utils
module D = Data
module S = Statistic
module P = Treeparameters


type t = {
  mutable size : float;
  mutable impurity : float;
  target_stats : Statistic.t array;
  attribute_stats : Statistic.t array;
}


let var_impurity var_stats =
  match var_stats with
  | S.Nominal counts ->
    let total = ref 0. in
    let sumsq = ref 0. in
    for i=0 to Array.length counts -1 do
      total := !total +. counts.(i);
      sumsq := !sumsq +. counts.(i)*.counts.(i)
    done;
    1. -. !sumsq /. (!total *. !total)
  | S.Numeric stats -> Statistic.variance stats
  | _ -> failwith "Unsupported variable type for trees"


let update_impurity parameters nodestats =
  let s = parameters.P.supervision in

  let target_impurity = ref 0. in
  for i=0 to Array.length nodestats.target_stats -1 do
    let var_imp = var_impurity nodestats.target_stats.(i) /. parameters.P.target_normalization.(i) in
    target_impurity := !target_impurity +. var_imp
  done;
  target_impurity := !target_impurity /. (float (Array.length nodestats.target_stats));

  let attribute_impurity = ref 0. in
  let importance_sum = ref 0. in
  if s < 1. then begin
    for i=0 to Array.length nodestats.attribute_stats -1 do
      let imp = parameters.P.importances.(i) in
      if imp > 0. then begin
        let var_imp = var_impurity nodestats.attribute_stats.(i) /. parameters.P.attribute_normalization.(i) in
        importance_sum := !importance_sum +. imp;
        attribute_impurity := !attribute_impurity +. var_imp *. imp
      end
    done;
    if !importance_sum > 0. then attribute_impurity := !attribute_impurity /. !importance_sum
  end;

  nodestats.impurity <- s *. !target_impurity +. (1. -. s) *. !attribute_impurity


let of_subset subset =
  let target_stats = match Statistic.subset_target_stats subset with
    | Statistic.Tuple (_,s) -> s
    | _ -> failwith "Subset statistic is not a Tuple"
  in
  let attribute_stats = match Statistic.subset_attribute_stats subset with
    | Statistic.Tuple (_,s) -> s
    | _ -> failwith "Subset statistic is not a Tuple"
  in
  {
    size = float (Array.length subset.D.samples);
    impurity = 1.;
    target_stats;
    attribute_stats;
  }



let add_example stats subset example parameters =
  stats.size <- stats.size +. 1.;
  for i=0 to Array.length subset.D.targets -1 do
    let d = D.get_datum subset example subset.D.targets.(i) in
    match d.D.value with
    | None -> ()
    | Some value -> begin
        match stats.target_stats.(i) with
        | S.Nominal counts ->
          let value = Int.of_float value in
          counts.(value) <- counts.(value)+.1.
        | S.Numeric stats ->
          stats.S.count <- stats.S.count +. 1.;
          stats.S.sum <- stats.S.sum +. value;
          stats.S.sumsq <- stats.S.sumsq +. value*.value;
        | _ -> failwith "Not supported variable structure"
      end
  done;

  if parameters.P.supervision < 1. then begin
    for i=0 to Array.length subset.D.attributes -1 do
      let d = D.get_datum subset example subset.D.attributes.(i) in
      match d.D.value with
      | None -> ()
      | Some value -> begin
          match stats.attribute_stats.(i) with
          | S.Nominal counts ->
            let value = Int.of_float value in
            counts.(value) <- counts.(value)+.1.
          | S.Numeric stats ->
            stats.S.count <- stats.S.count +. 1.;
            stats.S.sum <- stats.S.sum +. value;
            stats.S.sumsq <- stats.S.sumsq +. value*.value;
          | _ -> failwith "Not supported variable structure"
        end
    done;
  end


let remove_example stats subset example parameters =
  stats.size <- stats.size -. 1.;
  for i=0 to Array.length subset.D.targets -1 do
    let d = D.get_datum subset example subset.D.targets.(i) in
    match d.D.value with
    | None -> ()
    | Some value -> begin
        match stats.target_stats.(i) with
        | S.Nominal counts ->
          let value = Int.of_float value in
          counts.(value) <- counts.(value)-.1.
        | S.Numeric stats ->
          stats.S.count <- stats.S.count -. 1.;
          stats.S.sum <- stats.S.sum -. value;
          stats.S.sumsq <- stats.S.sumsq -. value*.value;
        | _ -> failwith "Not supported variable structure"
      end
  done;

  if parameters.P.supervision < 1. then begin
    for i=0 to Array.length subset.D.attributes -1 do
      let d = D.get_datum subset example subset.D.attributes.(i) in
      match d.D.value with
      | None -> ()
      | Some value -> begin
          match stats.attribute_stats.(i) with
          | S.Nominal counts ->
            let value = Int.of_float value in
            counts.(value) <- counts.(value)-.1.
          | S.Numeric stats ->
            stats.S.count <- stats.S.count -. 1.;
            stats.S.sum <- stats.S.sum -. value;
            stats.S.sumsq <- stats.S.sumsq -. value*.value;
          | _ -> failwith "Not supported variable structure"
        end
    done;
  end
