open Utils

let learner ~supervision ~minimum_node_size ~description ~subspaces =
  (module struct

    let log () = ""
    let description = description

    let induce task =
      let start_time = time () in
      let trainset = task.Task.data in
      let aggregate = Aggregation.calculate
          task.Task.target_distance
          (Statistic.subset_target_stats trainset)
      in
      let parameters = {
        Treeparameters.supervision = supervision;
        Treeparameters.minimum_node_size = minimum_node_size;
        Treeparameters.aggregate = aggregate;
        Treeparameters.attribute_normalization =
          Test.get_normalizations (Statistic.subset_attribute_stats trainset);
        Treeparameters.target_normalization =
          Test.get_normalizations (Statistic.subset_target_stats trainset);
        Treeparameters.importances = trainset.Data.importances;
        Treeparameters.subspaces = subspaces
      } in
      let tree = Tree.greedy_construction trainset (Nodestats.of_subset trainset) parameters in
      let total_time = (time ()) -. start_time in
      [|{
        Model.description = description;
        Model.to_string = (fun () -> Tree.to_string tree);
        Model.predict = (fun x -> Tree.predict tree x aggregate)
      }, total_time|]
  end : Algorithm.Modeler)
