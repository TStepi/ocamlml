open Utils
module D = Data
module NS = Nodestats
module P = Treeparameters

type t = {
  run : D.t -> bool option;
  description : string
}

type result = {
  test : t;
  heuristic : float;
  left_stats : NS.t;
  left_set : D.subset;
  right_stats : NS.t;
  right_set : D.subset
}


let acceptable parameters result =
  let size1 = Array.length result.left_set.D.samples in
  let size2 = Array.length result.right_set.D.samples in
  result.heuristic > 0. &&
  size1 >= parameters.P.minimum_node_size &&
  size2 >= parameters.P.minimum_node_size


let dummy_test = {
  run = (fun x -> None);
  description = "dummy"
}

let dummy_result subset starting_stats = {
  test = dummy_test;
  heuristic = 0.;
  left_stats = starting_stats;
  left_set = subset;
  right_stats = starting_stats;
  right_set = subset;
}


let get_normalizations tuple_stats =
  match tuple_stats with
  | Statistic.Tuple (_,s) -> Array.map s ~f:NS.var_impurity
  | _ -> failwith "Not a tuple subset structure"


let heuristic start_stats left_stats right_stats =
  start_stats.NS.impurity -.
  (left_stats.NS.size *. left_stats.NS.impurity +.
   right_stats.NS.size *. right_stats.NS.impurity) /.
  (left_stats.NS.size +. right_stats.NS.size)



let best_numeric_test subset attribute parameters starting_stats =
  (* sort the examples according to the attribute value *)
  Array.sort subset.D.samples ~cmp:(fun i j ->
      D.compare
        subset.D.dataset.D.values.(i).(attribute)
        subset.D.dataset.D.values.(j).(attribute)
    );

  let right_stats = {
    starting_stats with
    NS.target_stats = Array.map starting_stats.NS.target_stats ~f:Statistic.copy;
    NS.attribute_stats = Array.map starting_stats.NS.attribute_stats ~f:Statistic.copy
  } in

  let left_stats = {
    NS.size = 0.;
    NS.impurity = Float.infinity;
    NS.target_stats = Array.map starting_stats.NS.target_stats ~f:Statistic.empty_copy;
    NS.attribute_stats = Array.map starting_stats.NS.attribute_stats ~f:Statistic.empty_copy
  } in

  let index = ref 0 in
  (* Iterate missing values *)
  while !index < Array.length subset.D.samples &&
        D.is_missing (D.get_datum subset !index attribute) do
    NS.add_example left_stats subset !index parameters;
    incr index
  done;
  let last_missing = !index - 1 in

  if !index = Array.length subset.D.samples then dummy_result subset starting_stats else begin

    (* Evaluate possible splits *)
    let best_index = ref !index in
    let best_heuristic = ref 0. in
    let last_value = ref (Option.value_exn (D.get_datum subset !index attribute).D.value) in
    while !index < Array.length subset.D.samples - parameters.P.minimum_node_size + 1 do
      let value = Option.value_exn (D.get_datum subset !index attribute).D.value in
      if value > !last_value then begin
        last_value := value;
        NS.update_impurity parameters right_stats;
        NS.update_impurity parameters left_stats;
        let current_heuristic = heuristic starting_stats left_stats right_stats in
        if current_heuristic > !best_heuristic then
          (best_heuristic := current_heuristic; best_index := !index -1);
      end;
      NS.remove_example right_stats subset !index parameters;
      NS.add_example left_stats subset !index parameters;
      incr index
    done;

    if !best_heuristic = 0. then dummy_result subset starting_stats else begin

      decr index;
      while !index > !best_index do
        NS.remove_example left_stats subset !index parameters;
        NS.add_example right_stats subset !index parameters;
        decr index
      done;
      NS.update_impurity parameters right_stats;
      NS.update_impurity parameters left_stats;

      let v1 = Option.value_exn (D.get_datum subset !best_index attribute).D.value in
      let v2 = Option.value_exn (D.get_datum subset (!best_index +1) attribute).D.value in
      let split_point = (v1+.v2) /. 2. in

      let test = {
        run = (fun x ->
            if D.is_missing x then None else
              match x.D.components.(attribute).D.value with
              | None -> None
              | Some v -> Some (v > split_point));

        description = sprintf "%s > %.3f"
            (subset.D.dataset.D.variable_names.(attribute))
            split_point
      } in

      let test_right_size = Array.length subset.D.samples - !best_index - 1 in
      let right_set_samples = Array.append
          (Array.sub subset.D.samples 0 (last_missing+1))
          (Array.sub subset.D.samples (!best_index +1) test_right_size)
      in

      {
        test;
        heuristic = !best_heuristic;
        left_stats;
        left_set = {subset with D.samples = Array.sub subset.D.samples 0 (!best_index+1)};
        right_stats;
        right_set = {subset with D.samples = right_set_samples}
      }
    end
  end



let best_nominal_test subset attribute parameters starting_stats names =

  (* sort the examples according to the attribute value *)
  Array.sort subset.D.samples ~cmp:(fun i j ->
      D.compare
        subset.D.dataset.D.values.(i).(attribute)
        subset.D.dataset.D.values.(j).(attribute)
    );

  let left_stats = {
    starting_stats with
    NS.target_stats = Array.map starting_stats.NS.target_stats ~f:Statistic.copy;
    NS.attribute_stats = Array.map starting_stats.NS.attribute_stats ~f:Statistic.copy
  } in

  let right_stats = {
    NS.size = 0.;
    NS.impurity = Float.infinity;
    NS.target_stats = Array.map starting_stats.NS.target_stats ~f:Statistic.empty_copy;
    NS.attribute_stats = Array.map starting_stats.NS.attribute_stats ~f:Statistic.empty_copy
  } in

  let index = ref 0 in
  (* Iterate missing values *)
  while !index < Array.length subset.D.samples &&
        D.is_missing (D.get_datum subset !index attribute) do
    NS.add_example right_stats subset !index parameters;
    incr index
  done;
  let last_missing = !index - 1 in

  if !index = Array.length subset.D.samples then dummy_result subset starting_stats else begin

    let best_value = ref Float.nan in
    let best_heuristic = ref 0. in
    let chosen_value = ref (Option.value_exn (D.get_datum subset !index attribute).D.value) in
    let max_index = Array.length subset.D.samples in
    while !index < max_index do
      let value = Option.value_exn (D.get_datum subset !index attribute).D.value in
      if not (Float.equal value !chosen_value) then begin
        NS.update_impurity parameters right_stats;
        NS.update_impurity parameters left_stats;
        let current_heuristic = heuristic starting_stats left_stats right_stats in
        if current_heuristic > !best_heuristic then
          (best_heuristic := current_heuristic; best_value := !chosen_value);
        let rev_index = ref (!index -1) in
        while !rev_index >= 0 &&
              Option.value_exn (D.get_datum subset !rev_index attribute).D.value = !chosen_value do
          NS.add_example left_stats subset !rev_index parameters;
          NS.remove_example right_stats subset !rev_index parameters;
          decr rev_index
        done;
        chosen_value := value;
      end;
      NS.add_example right_stats subset !index parameters;
      NS.remove_example left_stats subset !index parameters;
      incr index
    done;

    if right_stats.NS.size >= float parameters.P.minimum_node_size then begin
      NS.update_impurity parameters right_stats;
      NS.update_impurity parameters left_stats;
      let current_heuristic = heuristic starting_stats left_stats right_stats in
      if current_heuristic > !best_heuristic then
        (best_heuristic := current_heuristic; best_value := !chosen_value);
      let rev_index = ref (!index -1) in
      while !rev_index >= 0 &&
            Option.value_exn (D.get_datum subset !rev_index attribute).D.value = !chosen_value do
        NS.add_example left_stats subset !rev_index parameters;
        NS.remove_example right_stats subset !rev_index parameters;
        decr rev_index
      done;
    end;

    if !best_heuristic = 0. then dummy_result subset starting_stats else begin

      let test = {
        run = (fun x ->
            if D.is_missing x then None else
              match x.D.components.(attribute).D.value with
              | None -> None
              | Some v -> Some (v = !best_value));

        description = sprintf "%s = %s"
            (subset.D.dataset.D.variable_names.(attribute))
            names.(Int.of_float !best_value)
      } in

      let right_samples = ref (List.init (last_missing+1) ~f:(fun i -> subset.D.samples.(i))) in
      let left_samples = ref (List.init (last_missing+1) ~f:(fun i -> subset.D.samples.(i))) in
      for i=(last_missing+1) to Array.length subset.D.samples -1 do
        let value = Option.value_exn (D.get_datum subset i attribute).D.value in
        if Float.equal value !best_value then
          (right_samples := subset.D.samples.(i) :: !right_samples;
           NS.add_example right_stats subset i parameters;
           NS.remove_example left_stats subset i parameters)
        else
          left_samples := subset.D.samples.(i) :: !left_samples
      done;
      NS.update_impurity parameters right_stats;
      NS.update_impurity parameters left_stats;

      {
        test;
        heuristic = !best_heuristic;
        left_stats;
        left_set = {subset with D.samples = List.to_array !left_samples};
        right_stats;
        right_set = {subset with D.samples = List.to_array !right_samples}
      }
    end
  end


let binary_test subset attribute parameters starting_stats =

  let left_stats = {
    NS.size = 0.;
    NS.impurity = Float.infinity;
    NS.target_stats = Array.map starting_stats.NS.target_stats ~f:Statistic.empty_copy;
    NS.attribute_stats = Array.map starting_stats.NS.attribute_stats ~f:Statistic.empty_copy
  } in

  let right_stats = {
    NS.size = 0.;
    NS.impurity = Float.infinity;
    NS.target_stats = Array.map starting_stats.NS.target_stats ~f:Statistic.empty_copy;
    NS.attribute_stats = Array.map starting_stats.NS.attribute_stats ~f:Statistic.empty_copy
  } in

  let right_samples = ref [] in
  let left_samples = ref [] in

  for i=0 to Array.length subset.D.samples -1 do
    match (D.get_datum subset i attribute).D.value with
    | None ->
      NS.add_example right_stats subset i parameters;
      NS.add_example left_stats subset i parameters;
      right_samples := subset.D.samples.(i) :: !right_samples;
      left_samples := subset.D.samples.(i) :: !left_samples;
    | Some 0. ->
      NS.add_example left_stats subset i parameters;
      left_samples := subset.D.samples.(i) :: !left_samples;
    | Some 1. ->
      NS.add_example right_stats subset i parameters;
      right_samples := subset.D.samples.(i) :: !right_samples;
    | _ -> failwith "Not a binary attribute in binary split"
  done;

  NS.update_impurity parameters right_stats;
  NS.update_impurity parameters left_stats;
  let heuristic = heuristic starting_stats left_stats right_stats in

  let test = {
    run = (fun x ->
        if D.is_missing x then None else
          match x.D.components.(attribute).D.value with
          | Some 0. -> Some false
          | Some 1. -> Some true
          | _ -> None);

    description = sprintf "%s = 1"
        (subset.D.dataset.D.variable_names.(attribute))
  } in

  {
    test;
    heuristic = heuristic;
    left_stats;
    left_set = {subset with D.samples = List.to_array !left_samples};
    right_stats;
    right_set = {subset with D.samples = List.to_array !right_samples}
  }





let best_attribute_test subset starting_stats parameters attribute =
  match subset.D.dataset.D.structures.(attribute) with
  | (Structure.Real | Structure.Integer) ->
    best_numeric_test subset attribute parameters starting_stats
  | Structure.Nominal values ->
    best_nominal_test subset attribute parameters starting_stats values
  | Structure.Binary ->
    binary_test subset attribute parameters starting_stats
  | _ -> failwith "Not splitting over anything other than nominal and numeric attributes (yet)"



let find_best_test subset starting_stats parameters =
  let chosen_atributes = random_subarray parameters.P.subspaces subset.D.attributes in
  let best_result = ref (dummy_result subset starting_stats) in
  for i=0 to Array.length chosen_atributes -1 do
    let result = best_attribute_test subset starting_stats parameters chosen_atributes.(i) in
    if result.heuristic > !best_result.heuristic then best_result := result
  done;

  if acceptable parameters !best_result then
    Some !best_result
  else
    None
