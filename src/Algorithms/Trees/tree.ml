open Utils


type t =
  | Split of split
  | Leaf of leaf

and split = {
  test : Test.t;
  left : t;
  right : t;
  split_size : float;
  split_impurity : float;
}

and leaf = {
  prototype : Data.t;
  leaf_size : float;
  leaf_impurity : float;
}


let predict tree sample aggregate =
  let rec predict' tree sample =
    match tree with
    | Leaf x -> x.prototype
    | Split x ->
      begin match x.test.Test.run sample with
        | Some true -> predict' x.right sample
        | Some false -> predict' x.left sample
        | None -> aggregate [|predict' x.right sample; predict' x.left sample|]
      end
  in
  predict' tree sample


let to_string tree =
  let acc = ref [] in
  let rec to_string prefix result t =
    match t with
    | Leaf l ->
      acc := (sprintf "%s%s%s ~ %.0f" prefix result (Data.to_string l.prototype) l.leaf_size) :: !acc
    | Split t ->
      acc := (sprintf "%s%s%s" prefix result t.test.Test.description) :: !acc;
      let padding = if String.length result = 0 then "" else "|      " in
      to_string (sprintf "%s%s" prefix padding) "+-yes: " t.right;
      to_string (sprintf "%s%s" prefix padding) "+--no: " t.left
  in
  to_string "" "" tree;
  String.concat ~sep:"\n" (List.rev !acc)


let rec greedy_construction subset starting_stats parameters =

  (*Find the best acceptable test, if it exists *)
  let best_test = Test.find_best_test
      subset
      starting_stats
      parameters
  in

  (*If you found an acceptable test, make a split and continue, otherwise make a leaf *)
  match best_test with
  | None ->
    let proto = parameters.Treeparameters.aggregate (Data.get_targets subset) in
    Leaf {
      prototype = proto;
      leaf_size = starting_stats.Nodestats.size;
      leaf_impurity = starting_stats.Nodestats.impurity
    }

  | Some result ->
    Split {
      test = result.Test.test;
      split_size = starting_stats.Nodestats.size;
      split_impurity = starting_stats.Nodestats.impurity;
      left = greedy_construction result.Test.left_set result.Test.left_stats parameters;
      right = greedy_construction result.Test.right_set result.Test.right_stats parameters;
    }
