open Utils

module BaselinePredictor : Algorithm.Modeler =
struct
  let description = ""
  let log () = ""
  let induce task =
    let trainset = task.Task.data in
    let centroid = Aggregation.calculate task.Task.target_distance
        (Statistic.subset_target_stats trainset)
        (Data.get_targets trainset)
    in
    [|{
      Model.description = "Baseline model";
      Model.to_string  = (fun () -> Data.to_string centroid);
      Model.predict = (fun _ -> centroid)
    }, 0.|]
end
