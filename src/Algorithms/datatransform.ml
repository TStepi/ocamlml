open Utils


let binary_tuple_to_set: Data.dataset -> int -> int -> Data.dataset =
  fun dataset start_attr stop_attr ->

    let new_structures = Array.init
        (Array.length dataset.Data.structures - stop_attr + start_attr)
        ~f:(fun i ->
            if i < start_attr then dataset.Data.structures.(i)
            else if i = start_attr then
              Structure.Set (Structure.Nominal (
                  Array.init (stop_attr - start_attr +1)
                    ~f:(fun i -> sprintf "Attr. %d" i)
                ))
            else dataset.Data.structures.(i+stop_attr-start_attr)
          )
    in

    let new_names = Array.init
        (Array.length dataset.Data.structures - stop_attr + start_attr)
        ~f:(fun i ->
            if i < start_attr then dataset.Data.variable_names.(i)
            else if i = start_attr then "Set"
            else dataset.Data.variable_names.(i+stop_attr-start_attr)
          )
    in

    let possible_elements = Array.init (stop_attr - start_attr +1)
        ~f:(fun i -> Data.nominal i)
    in
    let transform_value vars =
      Array.init (Array.length vars - stop_attr + start_attr)
        ~f:(fun i ->
            if i < start_attr then vars.(i)
            else if i = start_attr then
              Array.filteri possible_elements ~f:(fun j _ ->
                  vars.(i+j).Data.value = Some 1.
                ) |> Data.set
            else vars.(i+stop_attr - start_attr)
          )
    in

    {
      Data.name = dataset.Data.name;
      Data.structures = new_structures;
      Data.values = Array.map dataset.Data.values ~f:transform_value;
      Data.variable_names = new_names;
      Data.size = dataset.Data.size;
    }

