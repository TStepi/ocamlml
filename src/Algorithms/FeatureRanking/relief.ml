open Utils


let reliefF ~(k:int) ~(generator:Distancegenerator.t) ~(description:string) =
(module struct
  let description = description

  let logger = ref ["relieff"]
  let log () = String.concat ~sep:"\n" (List.rev !logger)

  let nearest data dist ind out k = 
    let candidates  = Array.filteri data ~f:(fun i d -> 
      i<>ind && Data.compare d.Data.output out = 0
    ) |> Array.map ~f:(fun d -> 
      (d.Data.input, d.Data.output, dist data.(ind).Data.input d.Data.input)
    ) in
    Array.sort candidates ~cmp:(fun d1 d2 -> Float.ascending (trd3 d1) (trd3 d2));
    Array.sub candidates 0 k
    

  let update_input_structure ~input_structure ~output_structure ~instances =
    match output_structure with
    | Structure.Nominal (values,frequences) -> begin
      let classes = Array.length values in
      let dist = generator input_structure in
      match input_structure with
      | Structure.Tuple (ws,ss) -> begin
        let dist = Distance.update (Data.update_structure input_structure (Data.get_inputs instances)) dist in
        let dists = Distance.get_components dist in
        let dist = Distance.interpret dist in
        let dists = Array.map dists ~f:Distance.interpret in
        let attrs = Array.length ws in
        let ranks = Array.init attrs ~f:(fun _ -> 0.) in
        let inst_num = Array.length instances in
        for i=0 to inst_num -1 do
          let my_components = instances.(i).Data.input.Data.components in 
          let my_class = Int.of_float (instances.(i).Data.output.Data.value) in
          for j=0 to classes -1 do
            let take = (Int.max 0)%(Int.min k)%Float.to_int%Float.round_down @@ frequences.(j)*.(float @@ inst_num) -. 1. in
            (*print_endline @@ sprintf "%.3f,%d,%d" !frequences.(j) inst_num take;*)
            let neighbours = nearest instances dist i {Data.value = float j; Data.components = [||] } take in
            let n = Array.length neighbours in 
            let factor = 
              if j = my_class then (-1.)
              else frequences.(j) /. (1. -. frequences.(my_class))
            in
            for l=0 to n-1 do
              let nei_components = (fst3 neighbours.(l)).Data.components in
              for a=0 to attrs-1 do
                ranks.(a) <- ranks.(a) +. factor *. (dists.(a) my_components.(a) nei_components.(a))
              done
            done
          done
        done;
        (*print_endline @@ List.to_string ~f:Float.to_string (Array.to_list ranks);*)
        Structure.Tuple (Array.map ranks ~f:(fun r -> (Float.max 0. r) /. float (k * Array.length instances) ),ss)
        end
      | _ -> failwith "Feature ranking only works on tuples."
    end
    | _ -> failwith "ReliefF works only for nominal output"

end : Algorithm.Ranker)
