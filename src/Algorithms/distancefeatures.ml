open Utils

let transform_dataset ~subset ~distance_generator ~rounds ~subspace ~anchors =

  let new_values = Array.init (Array.length subset.Data.samples) ~f:(fun _ -> []) in
  let new_dimension = ref 0 in

  for round=0 to rounds-1 do
    let set = Data.subspace_subset subspace subset in
    let attributes = Data.get_attributes set in
    let struc = Data.attribute_structure_of_subset set in
    let distances = distance_generator struc in
    let selected_anchors = random_elements anchors attributes in
    new_dimension := !new_dimension + (Array.length distances)*anchors;
    for d=0 to Array.length distances - 1 do
      let dist = Distance.interpret distances.(d) in
      for s=0 to Array.length attributes - 1 do
        for a=0 to anchors-1 do
          new_values.(s) <- (Data.real (dist attributes.(s) selected_anchors.(a))) :: new_values.(s)
        done
      done
    done
  done;

  let targets = Data.get_targets subset in
  let values = Array.mapi new_values ~f:(fun i lst ->
      Array.concat [List.to_array lst; targets.(i).Data.components]
    ) in

  let structures = Array.concat [
      initialize_array !new_dimension (Structure.Real (0.,0.,0.));
      Array.map subset.Data.targets ~f:(fun i -> subset.Data.dataset.Data.structures.(i))
    ] in

  let names = Array.concat [
      Array.init !new_dimension ~f:(fun i -> sprintf "D%d" i);
      Array.map subset.Data.targets ~f:(fun i -> subset.Data.dataset.Data.variable_names.(i))
    ] in

  {
    Data.name = sprintf "transformed_%s" subset.Data.dataset.Data.name;
    Data.values = values;
    Data.size = subset.Data.dataset.Data.size;
    Data.variable_names = names;
    Data.structures = structures
  }



