open Utils
open OUnit
module D = Data
module S = Structure

let s1 = S.Nominal [|"T1";"T2"|]
let s2 = S.Real
let s3 = S.Nominal [|"a";"b";"c"|]

let dataset = {
  D.name = "test dataset";
  D.size = 10;
  D.structures = [|s1;s2;s3|];
  D.variable_names = [|"var1";"var2";"var3"|];
  D.values =[|
    [|D.nominal 1; D.real 2.; D.nominal 1|];
    [|D.nominal 0; D.real 1.; D.nominal 2|];
    [|D.nominal 1; D.real 4.; D.nominal 1|];
    [|D.nominal 1; D.real 3.; D.nominal 1|];
    [|D.nominal 0; D.real 1.; D.nominal 2|];
    [|D.nominal 1; D.real 6.; D.nominal 1|];
    [|D.nominal 0; D.real 1.; D.nominal 0|];
    [|D.nominal 0; D.real 1.; D.nominal 0|];
    [|D.nominal 1; D.real 3.; D.nominal 1|];
    [|D.nominal 0; D.real 2.; D.nominal 0|];
  |]
}

let root_subset = {
  D.samples = [|0;1;2;3;4;5;6;7;8;9|];
  D.attributes = [|1;2|];
  D.importances = [|1.;1.|];
  D.targets = [|0|];
  D.dataset
}

let target_stats = Statistic.Tuple ([|1.|],[|
    Statistic.Nominal [|5.;5.|]
  |])

let attribute_stats = Statistic.Tuple ([|1.;1.|],[|
    Statistic.Numeric {Statistic.count = 10.; Statistic.sum = 24.; Statistic.sumsq = 82.};
    Statistic.Nominal [|3.;5.;2.|]
  |])


let run =
  "File statistics.ml" >:::
  [
    "subset_target_stats" >:: (fun () ->
        assert_equal
          ~printer:Statistic.to_string
          target_stats
          (Statistic.subset_target_stats root_subset);
      );

    "subset_attribute_stats" >:: (fun () ->
        assert_equal
          ~printer:Statistic.to_string
          attribute_stats
          (Statistic.subset_attribute_stats root_subset)
      );

    "copy" >:: (fun () ->
        assert_equal
          ~printer:Statistic.to_string
          attribute_stats
          (Statistic.copy attribute_stats)
      );

    "empty_copy" >:: (fun () ->
        assert_equal
          ~printer:Statistic.to_string
          (Statistic.Tuple ([|1.|],[|Statistic.Nominal [|0.;0.|]|]))
          (Statistic.empty_copy target_stats)
      )
  ]
