open Utils
open Dataarray
open OUnit

let datafy arr = Array.map arr ~f:(fun x -> Data.real x)

let run = "File dataarray.ml" >:::
[
  "data_array_min" >:: (fun () ->
    assert_equal
      Float.infinity
      (data_array_min [||]);
    assert_equal
      1.
      (data_array_min @@ datafy [|10.;1.;3.|])
  );

  "data_array_max" >:: (fun () ->
    assert_equal
      Float.neg_infinity
      (data_array_max [||]);
    assert_equal
      10.
      (data_array_max @@ datafy [|10.;1.;3.|])
  );

  "data_array_sum" >:: (fun () ->
      assert_equal
        10.
        (data_array_sum @@ datafy [|1.;4.;3.;2.|])
  );

  "data_array_mean" >:: (fun () ->
      assert_equal
        (10./.4.)
        (data_array_mean @@ datafy [|1.;4.;3.;2.|])
  );

  "data_array_variance" >:: (fun () ->
      assert_equal
        ~printer:Float.to_string
        2.
        (data_array_variance @@ datafy [|1.;2.;5.;3.;4.|])
  );

  "data_array_span" >:: (fun () ->
    assert_equal
      9.
      (data_array_span @@ datafy [|10.;1.;3.|])
  );

  "data_array_ranked" >:: (fun () ->
      assert_equal
        [|1.;2.5;4.;2.5|]
        (data_array_ranked @@ datafy [|1.;2.;3.;2.|])
  );

  "data_array_max_ind" >:: (fun () ->
      assert_equal
        3
        (data_array_max_ind @@ datafy [|10.;1.;5.;100.;99.|])
  );

  "data_array_min_ind" >:: (fun () ->
      assert_equal
        1
        (data_array_min_ind @@ datafy [|10.;1.;5.;100.;99.|])
  )
]
