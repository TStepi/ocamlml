open Utils
open OUnit

let run = "File floatarray.ml" >:::
[
  "initialize_array" >:: (fun () ->
      assert_equal
        [|1;1;1;1;1|]
        (initialize_array 5 1)
  );

  "float_array_min" >:: (fun () ->
    assert_equal
      Float.infinity
      (float_array_min [||]);
    assert_equal
      1.
      (float_array_min [|10.;1.;3.|])
  );

  "float_array_max" >:: (fun () ->
    assert_equal
      Float.neg_infinity
      (float_array_max [||]);
    assert_equal
      10.
      (float_array_max [|10.;1.;3.|])
  );

  "float_array_sum" >:: (fun () ->
      assert_equal
        10.
        (float_array_sum [|1.;4.;3.;2.|])
  );

  "float_array_mean" >:: (fun () ->
      assert_equal
        (10./.4.)
        (float_array_mean [|1.;4.;3.;2.|])
  );

  "float_array_variance" >:: (fun () ->
      assert_equal
        ~printer:Float.to_string
        2.
        (float_array_variance [|1.;2.;5.;3.;4.|])
  );

  "float_array_scale" >:: (fun () ->
      assert_equal
        [|2.;4.;6.;8.|]
        (let a = [|1.;2.;3.;4.|] in float_array_scale a 2.; a)
  );

  "float_array_normalize" >:: (fun () ->
      assert_equal
        [|0.25;1.;0.75;0.5|]
        (let a =  [|1.;4.;3.;2.|] in float_array_normalize a; a)
  );

  "float_array_make_convex" >:: (fun () ->
      assert_equal
        ~printer:((List.to_string ~f:Float.to_string)%Array.to_list)
        [|0.5;0.0;0.0;0.5|]
        (let a =  [|1.;0.;0.;1.|] in float_array_make_convex a; a)
  );

  "float_array_span" >:: (fun () ->
    assert_equal
      9.
      (float_array_span [|10.;1.;3.|])
  );

  "float_array_ranked" >:: (fun () ->
      assert_equal
        [|1.;2.5;4.;2.5|]
        (float_array_ranked [|1.;2.;3.;2.|])
  );

  "float_array_inf_scale" >:: (fun () ->
      assert_equal
        [|1.;2.;1.|]
        (let a = [|1.;2.;1.|] in float_array_inf_scale a; a);
      assert_equal
        [|0.;1.;0.|]
        (let a = [|1.;Float.infinity;100.|] in float_array_inf_scale a; a)
  );

  "float_array_max_ind" >:: (fun () ->
      assert_equal
        3
        (float_array_max_ind [|10.;1.;5.;100.;99.|])
  );

  "float_array_min_ind" >:: (fun () ->
      assert_equal
        1
        (float_array_min_ind [|10.;1.;5.;100.;99.|])
  );

  "float_array_translate" >:: (fun () ->
      assert_equal
        [|2.;3.;4.|]
        (let a = [|1.;2.;3.|] in float_array_translate a 1.; a)
  );

]
