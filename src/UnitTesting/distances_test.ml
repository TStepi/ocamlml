open Utils
open OUnit
open Data
open Distance

let delta = interpret Delta

let rampa =
  let m = Ramp 10. in
  interpret m

let powermean_max =
  let m = Powermean (1.,[|0.;0.;1.|],[|1.;1.;1.|],[|Ramp 10.; Delta; Delta; |]) in
  interpret m

let tupleavg =
  let m = Average ([|1.;1.;1.|],[|Ramp 10.; Delta; Delta|]) in
  interpret m

let tuplemax =
  let m = Maximum ([|1.;1.;1.|],[|Ramp 10.; Delta; Delta|])in
  interpret m

let tupleeuc =
  let m = Euclidean ([|1.;1.;1.|],[|Ramp 10.; Delta; Delta|]) in
  interpret m

let tuplemedian =
  let m = Median ([|1.;1.;1.|],[|Ramp 10.; Delta; Delta|]) in
  interpret m

let polna =
  let m = CompleteLink (Ramp 10.) in
  interpret m

let enojna =
  let m = SingleLink (Ramp 10.) in
  interpret m

let povp =
  let m = AverageLink (Ramp 10.) in
  interpret m

let haus =
  let m = Hausdorff (Ramp 10.) in
  interpret m

let dtw =
  let m = Warping (100,Ramp 10.) in
  interpret m

let lev =
  let m = Edit (Ramp 10.) in
  interpret m

let set_a = set [|integer 4; integer 1; integer 7; integer 5|]
let set_b = set [|integer 2; integer 1; integer 5|]
let seq_a = sequence [|integer 0; integer 1|]
let seq_b = sequence [|integer 0; integer 0; integer 1|]

let run = "File distances.ml" >:::
[
  "delta" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.
      (delta (nominal 1) (nominal 1));

    assert_equal
      ~printer:Float.to_string
      1.
      (delta (nominal 2) (nominal 1))
  );

  "rampa" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.5
      (rampa (real 4.) (real 9.))
  );

  "tuple" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      1.
      (powermean_max (tuple [|real 5.; nominal 1; nominal 3|]) (tuple [|real 7.; nominal 1; nominal 2|]));
    assert_equal
      ~printer:Float.to_string
      1.
      (tuplemax (tuple [|real 5.; nominal 1; nominal 3|]) (tuple [|real 7.; nominal 1; nominal 2|]));
    assert_equal
      ~printer:Float.to_string
      (1.2 /. 3.)
      (tupleavg (tuple [|real 5.; nominal 1; nominal 3|]) (tuple [|real 7.; nominal 1; nominal 2|]));
    assert_equal
      ~printer:Float.to_string
      (sqrt @@ 1.04 /. 3.)
      (tupleeuc (tuple [|real 5.; nominal 1; nominal 3|]) (tuple [|real 7.; nominal 1; nominal 2|]));
    assert_equal
      ~printer:Float.to_string
      0.2
      (tuplemedian (tuple [|real 5.; nominal 1; nominal 3|]) (tuple [|real 7.; nominal 1; nominal 2|]))
  );

  "jaccard" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.6
      ((interpret Jaccard) set_a set_b)
  );

  "enojna" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.
      (enojna set_a set_b)
  );

  "polna" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.6
      (polna set_a set_b)
  );

  "povprecna" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      (31./.120.)
      (povp set_a set_b)
  );

  "hausdorff" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.2
      (haus set_a set_b)
  );

  "warping" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.
      (dtw seq_b seq_a)
  );

  "levenshtein" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      (1./.3.)
      (lev seq_a seq_b)
  );

  "qualitative" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      (1./.3.)
      begin
        let d =  Qualitative 1.1 |> interpret in
        d
          (sequence [|integer 0; integer 1; integer 5|])
          (sequence [|integer 1; integer 2; integer 2|])
      end
  );


  "spearman" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      (0.5**0.5)
      ((interpret Spearman)
        (sequence [|integer 0; integer 1; integer 5|])
        (sequence [|integer 2; integer 2; integer 2|]))
  );

  "pearson" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      (0.5**0.5)
      ((interpret Pearson)
        (sequence [|integer 0; integer 1; integer 5|])
        (sequence [|integer 2; integer 2; integer 2|]))
  );
  ]
