open OUnit
open Utils

let run () =
  ignore @@ List.map ~f:(run_test_tt )
    [
      Floatarray_test.run;
      (* Dataarray_test.run; *)
      Utils_test.run;
      Data_test.run;
      Distances_test.run;
      Aggregation_test.run;
      Statistic_test.run;
      Split_test.run;
      Measure_test.run
    ]
