open Utils
open OUnit
open Data
module S = Structure

let s1 = S.Nominal [|"a";"b";"c"|]
let s2 = S.Integer
let s3 = S.Real

let dataset = {
  name = "test dataset";
  size = 10;
  structures = [|s1;s2;s3|];
  variable_names = [||];
  values =[|
    [|Data.nominal 0; Data.integer 2; Data.real 1.|];
    [|Data.nominal 1; Data.integer 1; Data.real 3.|];
    [|Data.nominal 1; Data.integer 4; Data.real 1.|];
    [|Data.nominal 2; Data.integer 1; Data.real 1.|];
    [|Data.nominal 1; Data.integer 3; Data.real 2.|];
    [|Data.nominal 2; Data.integer 6; Data.real 1.|];
    [|Data.nominal 0; Data.integer 1; Data.real 3.|];
    [|Data.nominal 1; Data.integer 3; Data.real 3.|];
    [|Data.nominal 2; Data.integer 1; Data.real 1.|];
    [|Data.nominal 0; Data.integer 2; Data.real 0.|];
  |]
}

let root_subset = {
  samples = [|0;1;2;3;4;5;6;7;8;9|];
  attributes = [|2|];
  importances = [|1.|];
  targets = [|0|];
  dataset
}

let subset = {
  samples = [|0;1;3;4;5;6;9|];
  attributes = [|2|];
  importances = [|1.|];
  targets = [|0|];
  dataset
}

let subset_attributes = [|
  tuple [|Data.real 1.|];
  tuple [|Data.real 3.|];
  tuple [|Data.real 1.|];
  tuple [|Data.real 2.|];
  tuple [|Data.real 1.|];
  tuple [|Data.real 3.|];
  tuple [|Data.real 0.|];
|]

let subset_targets = [|
  tuple [|Data.nominal 0|];
  tuple [|Data.nominal 1|];
  tuple [|Data.nominal 2|];
  tuple [|Data.nominal 1|];
  tuple [|Data.nominal 2|];
  tuple [|Data.nominal 0|];
  tuple [|Data.nominal 0|];
|]

let run = "File data.ml" >:::
[
  "has_structure" >:: (fun () ->
      assert_equal
        true
        (has_structure
           (S.Nominal [|"keks";"smeks";"fleks"|])
           {value=Some 1.; components = [||]; reliability = 1.});
      assert_equal
        false
        (has_structure
           (S.Nominal [|"keks";"smeks";"fleks"|])
           {value=Some 5.; components = [||]; reliability = 1.});
    );

  "get_attributes" >:: (fun () ->
      assert_equal
        subset_attributes
        (get_attributes subset)
    );

  "get_targets" >:: (fun () ->
      assert_equal
        subset_targets
        (get_targets subset)
    );

  "subset_complement" >:: (fun () ->
      assert_equal
        [|2;7;8|]
        (let a = (subset_complement root_subset subset).samples in Array.sort a ~cmp:Int.ascending; a)
    );

  "fold_subset" >:: (fun () ->
      assert_equal
        [|1;9|]
        (fold_subset subset 5 2).samples
    );

  (* Dela, ampak teži zaradi zaokroževanja *)
  (* "attribute_structure_of_subset" >:: (fun () -> *)
  (*     assert_equal *)
  (*       ~printer:S.to_string *)
  (*       (S.Tuple ([|1.|],[|S.Real (11./.7.,54./.49.,3.) |])) *)
  (*       (attribute_structure_of_subset subset) *)
  (*   ); *)

  "target_structure_of_subset" >:: (fun () ->
      assert_equal
        (S.Tuple [|S.Nominal [|"a";"b";"c"|]|])
        (target_structure_of_subset subset)
    );
]
