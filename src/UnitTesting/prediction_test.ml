open Utils
open OUnit

let dg = Distancegenerator.construct_pool
  ~tuples:[|Distancegenerator.Average|]
  ~series:[||]
  ~sets:[||]
  ~sequences:[||]

let out_dg = Distancegenerator.construct
  ~tuple:Distancegenerator.Average
  ~seria:Distancegenerator.Qualitative
  ~set:Distancegenerator.Hausdorff
  ~sequence:Distancegenerator.Edit

let measure_selector struc generator =
  match struc with
  | Structure.Nominal (names,freq) -> Measure.precision names
  | (Structure.Real _ | Structure.Integer _ ) -> Measure.rrse
  | _ -> 
    let dist = generator struc in 
    Measure.asd (Distance.interpret dist)

module Average3NN = (val Knn.construct ~startk:3 ~description:"Tester" ~distance_generator:dg : Algorithm.Predictor)

let avg_prediction1 =
  let sc = Structure.Nominal ([||],[||]) in
  let sa = Structure.Nominal ([||],[||]) in

  let train_set = {
    Data.indices = [|1;2;3;0|];
    Data.inputs = [|Data.nominal 1; Data.nominal 1; Data.nominal 1; Data.nominal 2|];
    Data.outputs = [|Data.nominal 2; Data.nominal 3; Data.nominal 2; Data.nominal 3|]
  }
 in

  Average3NN.learn ~input_structure:sa ~output_structure:sc ~output_distance_generator:out_dg ~measure_selector ~trainset:train_set;
  Average3NN.predict (Data.nominal 1)

let avg_prediction2 =
  let sc = Structure.Nominal ([||],[||]) in
  let sa = Structure.Tuple ([|1.;1.|],[|Structure.Nominal ([||],[||]); Structure.Integer (0.,0.,99.)|]) in

  let train_set = {
    Data.indices = [|1;2;3;0|];
    Data.inputs = [|
      Data.tuple [|Data.nominal 1; Data.integer 1|];
      Data.tuple [|Data.nominal 1; Data.integer 3|];
      Data.tuple [|Data.nominal 2; Data.integer 1|];
      Data.tuple [|Data.nominal 2; Data.integer 100|]
    |];
    Data.outputs = [|Data.nominal 1; Data.nominal 2; Data.nominal 2; Data.nominal 1|]
  }
 in
  Average3NN.learn ~input_structure:sa ~output_structure:sc ~output_distance_generator:out_dg ~measure_selector ~trainset:train_set;
  Average3NN.predict (Data.tuple [|Data.nominal 1; Data.integer 1|])


let run = "File prediction.ml" >:::
[
  "knn with delta and k=3" >:: (fun () ->
    assert_equal
      ~printer:Data.to_string
      (Data.nominal 2)
      avg_prediction1
  );

  "knn with avg and k=3" >:: (fun () ->
    assert_equal
      ~printer:Data.to_string
      (Data.nominal 1)
      avg_prediction2
  )
]
