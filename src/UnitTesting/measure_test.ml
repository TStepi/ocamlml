open Utils
open OUnit
module D = Data
module M = Measure

let data d1 d2 = D.integer d1
let data2 v = D.real v

let get_value performance =
  match performance with
  | M.Single (_,v) -> v
  | M.PerComponent _ -> failwith "No value to get here"

let predictions = [|
  [data 1 1; data 1 3];
  [data 2 1; data 2 3];
  [data 4 1; data 4 3];
  [data 1 1; data 1 3];
  [data 1 1; data 1 3];
  [data 2 1; data 2 3];
  [data 1 1; data 1 3];
  [data 3 1; data 3 3];
  [data 1 1; data 1 3];
  [data 3 1; data 3 3];
|]

let solutions = [|
  data 2 2;
  data 2 2;
  data 4 2;
  data 1 2;
  data 1 2;
  data 3 2;
  data 1 2;
  data 3 2;
  data 2 2;
  data 1 2;
|]

let predictions2 = [|
  [data2 0.5; data2 0.3];
  [data2 0.8; data2 0.8];
  [data2 0.; data2 0.3];
  [data2 0.9; data2 0.8];
  [data2 0.6; data2 0.9];
  [data2 0.7; data2 0.4];
  [data2 0.5; data2 0.4];
  [data2 0.2; data2 0.3];
  [data2 0.; data2 0.6];
  [data2 1.; data2 0.3];
|]

let solutions2 = [|
  data2 1.;
  data2 1.;
  data2 0.;
  data2 1.;
  data2 1.;
  data2 1.;
  data2 0.;
  data2 0.;
  data2 0.;
  data2 1.;
|]

let ad = M.ad (Distance.interpret (Distance.Ramp 2.))
let rasd = M.rasd (Distance.interpret (Distance.Ramp 1. ))
let precision = M.precision [|"1";"2";"3";"4";""|]
let recall = M.recall [|"";"";"";"";""|]
let f1 = M.f1 [|"";"";"";"";""|]

let run = "File measure.ml" >:::
[
  "relative misses" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.4
      (get_value @@ M.relative_misses predictions solutions);
  );

  "mae" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.5
      (get_value @@ M.mae (-1.) predictions solutions)
  );

  "rmse" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      (sqrt 0.7)
      (get_value @@ M.rmse (-1.) predictions solutions)
  );

  "mse" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.7
      (get_value @@ M.mse predictions solutions);
  );

  "bias" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.7
      (get_value @@ M.mse_bias predictions solutions);
  );

  "variance" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.
      (get_value @@ M.mse_var predictions solutions);
    );

  "ad" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.25
      (get_value @@ ad predictions solutions)
  );

  "rasd" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      (sqrt 0.4)
      (get_value @@ rasd predictions solutions)
  );

  "precision" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      (1. -. 0.59)
      (get_value @@ precision predictions solutions)
  );

  "recall" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      0.4
      (get_value @@ recall predictions solutions)
  );

  "f1" >:: (fun () ->
    assert_equal
      ~printer:Float.to_string
      (1. -. 44./.75.)
      (get_value @@ f1 predictions solutions)
  );

  "binary recall" >:: (fun () ->
      assert_equal
        ~printer:Float.to_string
        0.25
        (get_value @@ M.binary_recall 0.5 predictions2 solutions2)
    );

  (* "binary precision" >:: (fun () -> *)
  (*     assert_equal *)
  (*       ~printer:Float.to_string *)
  (*       (1. -. 26./.33.) *)
  (*       (get_value @@ M.binary_precision 0.5 predictions2 solutions2) *)
  (*   ); *)

  "binary f1" >:: (fun () ->
      assert_equal
        ~printer:Float.to_string
        (1. -. 100. /. 133.)
        (get_value @@ M.binary_f1 0.5 predictions2 solutions2)

    )

]
