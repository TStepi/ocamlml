open Utils
open OUnit

let run = "File utils.ml" >:::
[
  "composition" >:: (fun () ->
      assert_equal
        98
        (((fun x -> x - 2) % (fun x -> x*x)) 10)
    );

  "cartesian_product_multiple" >:: (fun () ->
    assert_equal
      ~printer:(List.to_string ~f:(fun row -> List.to_string ~f:Int.to_string row))
      (List.sort ~cmp:compare [[1;3;6]; [1;4;6]; [1;5;6]; [2;3;6]; [2;4;6]; [2;5;6]])
      (List.sort ~cmp:compare @@ cartesian_product_multiple [[1;2]; [3;4;5]; [6]])
  );

  "index_of_exn" >:: (fun () ->
    assert_equal
      2
      (index_of_exn "3" [|"1";"2";"3"|])
  );

  "split_exn" >:: (fun () ->
    assert_equal
      ([1;3],2)
      (split_exn [1;2;3] 1);

    assert_equal
      ([2;3],1)
      (split_exn [1;2;3] 0);

    assert_equal
      ([1;2],3)
      (split_exn [1;2;3] 2)
  );

  "min3" >:: (fun () ->
    assert_equal
      1.
      (min_3float 1. 2. 3.);
    assert_equal
      1.
      (min_3float 2. 1. 3.);
    assert_equal
      1.
      (min_3float 3. 2. 1.);
  );

  "bounded_to" >:: (fun () ->
    assert_equal
      2.
      (bounded_to 2. 3. 0.);
    assert_equal
      3.
      (bounded_to 2. 3. 10.);
  );

  "find_complement" >:: (fun () ->
      assert_equal
        ~printer:(fun x -> List.to_string ~f:Int.to_string x)
        [5;3;1]
        (find_complement [1;2;3;4;5] [2;4])
    );

  "partialsort" >:: (fun () ->
      assert_equal
        [|1.;2.;3.|]
        (let a = [|2.;3.;1.|] in partialsort a 3 Float.ascending; a)
    );

  "float_list_sum" >:: (fun () ->
      assert_equal
        10.
        (float_list_sum [1.;4.;3.;2.])
    );
]
