open Utils
open OUnit

module A = Aggregation
module D = Data

let voted = A.calculate
    (Distance.Average ([|1.|],[|Distance.Delta|]))
    (Statistic.Tuple ([||],[|Statistic.Nominal [|0.1;0.1;0.1;0.1;0.1;0.1;0.4|]|]))

let num_stats = {Statistic.sum = 1.; Statistic.sumsq = 2.; Statistic.count = 1.}

let num_mean = A.calculate Distance.Delta (Statistic.Numeric num_stats)
(* let int_mean = A.calculate Distance.Delta (Structure.Integer) *)
(* let medoid = A.calculate Distance.Jaccard (Structure.Set (Structure.Integer)) *)

let tupefy arr = Array.map arr ~f:(fun x -> D.tuple [|x|])

let run = "File aggregation.ml" >:::
[
  "voted" >:: (fun () ->
    assert_equal
      ~printer:D.to_string
      (D.tuple [|D.nominal ~reliability:0.5 0|])
      (voted (tupefy [|D.nominal 0; D.nominal 1; D.nominal 2; D.nominal 0|]));
    assert_equal
      ~printer:D.to_string
      (D.tuple [|D.nominal ~reliability:0.7 1|])
      (voted (tupefy [|D.nominal ~reliability:0.3 0; D.nominal ~reliability:0.7 1|]));
    assert_equal
      ~printer:D.to_string
      (D.tuple [|D.nominal ~reliability:0.5 0|])
      (voted (tupefy [|D.nominal ~reliability:0.5 0; D.nominal ~reliability:0.5 1|]));
   );

  "real_mean" >:: (fun () ->
    assert_equal
      ~printer:D.to_string
      (D.real ~reliability:0. 4.)
      (num_mean [|D.real ~reliability:1. 7.; D.real ~reliability:1. 1.|]);
    assert_equal
      ~printer:D.to_string
      (D.real ~reliability:1. 7.)
      (num_mean [|D.real ~reliability:1. 7.; D.real ~reliability:0. 1.|]);
  );

  (* "int_mean" >:: (fun () -> *)
  (*   assert_equal *)
  (*     ~printer:D.to_string *)
  (*     (D.integer 4) *)
  (*     (int_mean [|1.;1.|] [|D.integer 7; D.integer 1|]); *)
  (*   assert_equal *)
  (*     ~printer:D.to_string *)
  (*     (D.integer 4) *)
  (*     (int_mean [|Float.infinity; Float.infinity|] [|D.integer 7;D.integer 1|]); *)
  (* ); *)

  (* "medoid" >:: (fun () -> *)
  (*   assert_equal *)
  (*     ~printer:D.to_string *)
  (*     (D.set [|D.nominal 0; D.nominal 1|]) *)
  (*     (medoid [|Float.infinity; 1.; 1.|] [|D.set [|D.nominal 0; D.nominal 1|]; D.set [|D.nominal 2; D.nominal 1|]; D.set [|D.nominal 2; D.nominal 1|]|]) *)
  (* ); *)
]
