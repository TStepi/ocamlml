open Utils
open OUnit
module D = Data
module S = Structure
module NS = Nodestats
module P = Treeparameters
module T = Test

let s1 = S.Nominal [|"T1";"T2"|]
let s2 = S.Real
let s3 = S.Nominal [|"a"; "b"; "c"|]

let dataset = {
  D.name = "test dataset";
  D.size = 10;
  D.structures = [|s1;s2;s3|];
  D.variable_names = [|"var1";"var2";"var3"|];
  D.values =[|
    [|D.nominal 1; D.real 2.; D.nominal 1|];
    [|D.nominal 0; D.real 1.; D.nominal 2|];
    [|D.nominal 1; D.real 4.; D.nominal 1|];
    [|D.nominal 1; D.real 3.; D.nominal 1|];
    [|D.nominal 0; D.real 1.; D.nominal 2|];
    [|D.nominal 1; D.real 6.; D.nominal 1|];
    [|D.nominal 0; D.real 1.; D.nominal 0|];
    [|D.nominal 0; D.real 1.; D.nominal 0|];
    [|D.nominal 1; D.real 3.; D.nominal 1|];
    [|D.nominal 0; D.real 2.; D.nominal 0|];
  |]
}

let root_subset1 = {
  D.samples = [|0;1;2;3;4;5;6;7;8;9|];
  D.attributes = [|2|];
  D.importances = [|1.|];
  D.targets = [|0|];
  D.dataset
}
let root_stats1 = Nodestats.of_subset root_subset1

let root_subset2 = {
  D.samples = [|0;1;2;3;4;5;6;7;8;9|];
  D.attributes = [|1|];
  D.importances = [|1.|];
  D.targets = [|0|];
  D.dataset
}
let root_stats2 = Nodestats.of_subset root_subset2

let get_normalizations tuple_stats =
  match tuple_stats with
  | Statistic.Tuple (_,s) -> Array.map s ~f:NS.var_impurity
  | _ -> failwith "Not a tuple subset structure"

let parameters1 = {
  P.supervision = 1.;
  P.minimum_node_size = 2;
  P.aggregate = Aggregation.calculate Distance.Delta (Statistic.subset_target_stats root_subset1);
  P.attribute_normalization = get_normalizations (Statistic.subset_attribute_stats root_subset1);
  P.target_normalization = get_normalizations (Statistic.subset_target_stats root_subset1);
  P.importances = root_subset1.D.importances;
  P.subspaces = All
}

let parameters2 = {
  P.supervision = 1.;
  P.minimum_node_size = 2;
  P.aggregate = Aggregation.calculate Distance.Delta (Statistic.subset_target_stats root_subset2);
  P.attribute_normalization = get_normalizations (Statistic.subset_attribute_stats root_subset2);
  P.target_normalization = get_normalizations (Statistic.subset_target_stats root_subset2);
  P.importances = root_subset2.D.importances;
  P.subspaces = All
}


let best_test1 = Option.value_exn (Test.find_best_test root_subset1 root_stats1 parameters1)
let best_test2 = Option.value_exn (Test.find_best_test root_subset2 root_stats2 parameters2)
let best_test3 = Test.find_best_test root_subset1 root_stats1 {parameters1 with P.minimum_node_size = 20}



let run =
  "Tree test related" >:::
  [
    "Normalization weights" >:: (fun () ->
        assert_equal
          ~printer:Float.to_string
          0.5
          parameters1.P.target_normalization.(0);

        assert_equal
          ~printer:Float.to_string
          0.62
          parameters1.P.attribute_normalization.(0);

        assert_equal
          ~printer:Float.to_string
          2.44
          parameters2.P.attribute_normalization.(0)
      );

    "Best test" >:: (fun () ->
        assert_equal
          ~printer:ident
          "var3 = b"
          best_test1.T.test.T.description;

        assert_equal
          ~printer:ident
          "var2 > 1.500"
          best_test2.T.test.T.description;

        assert_equal
          None
          best_test3;

        assert_equal
          ~printer:(array_to_string ~f:Int.to_string)
          [|1;4;6;7;9|]
          (Array.sorted_copy ~cmp:Int.ascending best_test1.T.left_set.D.samples);

        assert_equal
          ~printer:(array_to_string ~f:Int.to_string)
          [|0;2;3;5;8|]
          (Array.sorted_copy ~cmp:Int.ascending best_test1.T.right_set.D.samples);

        assert_equal
          ~printer:(array_to_string ~f:Int.to_string)
          [|1;4;6;7|]
          (Array.sorted_copy ~cmp:Int.ascending best_test2.T.left_set.D.samples);

        assert_equal
          ~printer:(array_to_string ~f:Int.to_string)
          [|0;2;3;5;8;9|]
          (Array.sorted_copy ~cmp:Int.ascending best_test2.T.right_set.D.samples);
      )
  ]
