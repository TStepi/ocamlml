open Utils
module DG = Distancegenerator

type t = {
  dataset_path : string;
  testset_path : string;
  report_path : string;
  threshold : float;
  disabled : int array;
  targets : int array;
  franks : bool;
}


let read_settings path =
  let lines = In_channel.read_lines path |> List.to_array in
  {
    dataset_path = lines.(0);
    testset_path = lines.(1);
    report_path = lines.(2);
    threshold = Float.of_string lines.(3);
    disabled =
      if String.equal lines.(4) "" then [||]
      else
        String.split lines.(4) ~on:','
        |> List.map ~f:Int.of_string |> List.to_array;
    targets = String.split lines.(5) ~on:','
              |> List.map ~f:Int.of_string |> List.to_array;
    franks = Bool.of_string lines.(6);
  }


(* Where to save reports *)
(* let dataset_path = "../e8datasets/STC/Binary/abalone.arff" *)
let comments = "5 runs of 10-fold Xval"

let threshold = 0.5

(* Distance used on output structure *)
let output_distance_generator =
  DG.construct ~tuple:DG.Average ~seria:DG.Spearman ~sequence:DG.Edit ~set:DG.Jaccard


(* Performance measure that algorithms optimize when choosing parameters *)
let rec optimisation_measure structure stats =
  match structure,stats with
  | Structure.Nominal names, _-> Measure.f1 names
  | Structure.Binary, _ -> Measure.binary_f1 threshold
  | (Structure.Real | Structure.Integer ), _ -> Measure.rmse (-1.)
  | Structure.Tuple structs, Statistic.Tuple (_,stats) ->
    Measure.component_average (Array.map2_exn structs stats optimisation_measure)
  | _, _ ->
    let dist = output_distance_generator stats in
    Measure.ad (Distance.interpret dist)


(* Performance measures to include in the report *)
let rec report_measures t struc stats =
  match struc,stats with
  | Structure.Nominal names, _ ->
    [|Measure.recall names; Measure.precision names; Measure.f1 names|]
  | Structure.Binary, _ ->
    [|Measure.binary_precision threshold; Measure.binary_recall threshold; Measure.binary_f1 threshold |]
  | (Structure.Real | Structure.Integer ), _ ->
    [|Measure.mse; Measure.mse_bias; Measure.mse_var;
      Measure.mae (-1.); Measure.rmse (-1.); Measure.mae 0.5; Measure.rmse 0.5;
      Measure.mae 0.6; Measure.rmse 0.6; Measure.mae 0.7; Measure.rmse 0.7;
      Measure.mae t; Measure.rmse t; Measure.mae 0.9; Measure.rmse 0.9;
      Measure.mae 0.95; Measure.rmse 0.95; Measure.mae 0.99; Measure.rmse 0.99;
      Measure.count Float.neg_infinity;
      Measure.count (-1.); Measure.count 0.5; Measure.count 0.6; Measure.count 0.7;
      Measure.count 0.8; Measure.count 0.9; Measure.count 0.95; Measure.count 0.99
    |]
  | Structure.Tuple structs, Statistic.Tuple (_,stats) ->
    [|Measure.per_component (Array.map2_exn structs stats (report_measures t))|]
  | _ ->
    let dist = output_distance_generator stats in
    [|Measure.ad (Distance.interpret dist); Measure.rasd (Distance.interpret dist) |]


(* use weka to calculate feature importance *)
let get_ranks dataset_path =
  ignore @@ Unix.system @@ String.concat ~sep:" "
    [
      "java -cp";
      "weka.jar:.";
      "Relief";
      dataset_path;
    ];
  let result = In_channel.read_lines "feature_ranks.txt"
               |> List.map ~f:Float.of_string
               |> List.map ~f:(max 0.)
               |> List.to_array in
  Sys.remove "feature_ranks.txt";
  print_endline "Finished rank calculation";
  result



(* which method of validation to use *)
let validation s =
  if String.length s.testset_path > 4 then begin
    let test_dataset = Reader.load s.testset_path in
    let test_subset = Data.create_root_subset test_dataset s.disabled s.targets in
    Evaluation.Test_set test_subset
  end else
    (* Evaluation.RandomSplit (10,0.1) *)
    Evaluation.CrossValidation (1,10)



(* algorithms to evaluate *)

let knn1 = Knn.basic ~k:1 ~description:"Knn-Avg-1"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Average)


let knn2 = Knn.basic ~k:3 ~description:"Knn-Avg-3"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Average)


let knn3 = Knn.basic ~k:5 ~description:"Knn-Avg-5"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Average)


let knn4 = Knn.basic ~k:7 ~description:"Knn-Avg-7"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Average)


let knn5 = Knn.basic ~k:9 ~description:"Knn-Avg-9"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Average)


let knn6 = Knn.basic ~k:1 ~description:"Knn-Euc-1"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Euclidean)


let knn7 = Knn.basic ~k:3 ~description:"Knn-Euc-3"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Euclidean)


let knn8 = Knn.basic ~k:5 ~description:"Knn-Euc-5"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Euclidean)


let knn9 = Knn.basic ~k:7 ~description:"Knn-Euc-7"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Euclidean)


let knn0 = Knn.basic ~k:9 ~description:"Knn-Euc-9"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Euclidean)

let knn10 = Knn.basic ~k:11 ~description:"Knn-Euc-11"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Euclidean)


let knn11 = Knn.basic ~k:15 ~description:"Knn-Euc-15"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Euclidean)


let knn12 = Knn.basic ~k:21 ~description:"Knn-Euc-21"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Euclidean)


let knn13 = Knn.basic ~k:61 ~description:"Knn-Euc-61"
    ~distance_generator:(DG.construct
                           ~seria:DG.EditS
                           ~sequence:DG.Edit
                           ~set:DG.Jaccard
                           ~tuple:DG.Euclidean)



let pct = Pct.learner
    ~supervision:1.
    ~minimum_node_size:2
    ~description:"Supervised PCT"
    ~subspaces:All

let random_pct = Pct.learner
    ~supervision:1.
    ~minimum_node_size:2
    ~description:"Supervised random PCT"
    ~subspaces:Sqrt


let pct_rforest = Algorithm.bagging_ensemble random_pct 100


(* let gpct = Genetictree.evolve *)
(*     ~supervision:1. *)
(*     ~size_matters:0.05 *)
(*     ~generations:300 *)
(*     ~interval:3000 *)
(*     ~population_size:33 *)
(*     ~description:"Evolutionary induction of PCTs" *)


let algorithm_list = [|
  (module Baseline.BaselinePredictor : Algorithm.Modeler);
  (* knn1; *)
  (* knn2; *)
  (* knn3; *)
  (* knn4; *)
  (* knn5; *)
  (* knn6; *)
  (* knn7; *)
  (* knn8; *)
  (* knn9; *)
  (* knn0; *)
  (* knn10; *)
  (* knn11; *)
  (* knn12; *)
  (* knn13; *)
  pct_rforest;
  random_pct
  (* gpct *)
|]
