open Utils

type performance =
  | Single of string*float
  | PerComponent of performance array array

type t = Data.t list array -> Data.t array -> performance


val relative_misses : t

val per_component : t array array -> t
val component_average : t array -> t

val count : float -> t
val mae : float -> t
val mse : t
val mse_var : t
val mse_bias : t
val rmse : float -> t

val ad : Distance.t -> t
val rasd : Distance.t -> t

val precision : string array -> t
val recall : string array -> t
val f1 : string array -> t

val binary_precision : float -> t
val binary_recall : float -> t
val binary_f1 : float -> t
