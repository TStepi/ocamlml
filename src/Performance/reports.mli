open Utils


val init :
  report_path: string ->
  data: Data.subset ->
  comments: string ->
  unit

val model_report :
  ?show_model: bool ->
  report_path: string ->
  measures: Measure.t ->
  result: Evaluation.result ->
  unit

(* val ranker_report : *)
(*   report_path:string -> *)
(*   algorithm:(module Algorithm.Ranker) -> *)
(*   result:Structure.t -> *)
(*   time:float -> *)
(*   unit *)
