open Utils

type result = {
  model : Model.t;
  induction_time : time;
  mutable predicting_time : time;
  real_values : Data.t array;
  predictions : Data.t list array;
}


type t =
  | Test_set of Data.subset
  | CrossValidation of int*int
  | RandomSplit of int*subarray_size


let update_result testset model result =
  let start_time = time () in
  Array.iter testset.Data.samples ~f:(fun i ->
      let prediction = model.Model.predict (Data.get_attribute testset i) in
      result.predictions.(i) <- prediction :: result.predictions.(i);
    );
  let total_time = time () -. start_time in
  result.predicting_time <- result.predicting_time +. total_time



let random_split_validation rounds ratio task algorithms results =
  let data = task.Task.data in
  for _ = 1 to rounds do
    let testset = Data.random_subset ratio data in
    let trainset = Data.subset_complement data testset in
    let models = Array.map algorithms ~f:(fun a ->
          let module A = (val a : Algorithm.Modeler) in
          A.induce {task with Task.data = trainset}
        ) |> Array.concat_map ~f:ident
      in
      Array.iter2_exn models results ~f:(fun (m,_) r -> update_result testset m r)
  done



let cross_validation rounds folds task algorithms results =
  let data = task.Task.data in
  for round = 1 to rounds do
    Array.permute data.Data.samples;
    for fold=1 to folds do
      let testset = Data.fold_subset data folds fold in
      let trainset = Data.subset_complement data testset in
      let models = Array.map algorithms ~f:(fun a ->
          let module A = (val a : Algorithm.Modeler) in
          A.induce {task with Task.data = trainset}
        ) |> Array.concat_map ~f:ident
      in
      Array.iter2_exn models results ~f:(fun (m,_) r -> update_result testset m r)
    done
  done



let evaluate procedure task algorithms =
  let full_models = Array.map algorithms ~f:(fun a ->
      let module A = (val a : Algorithm.Modeler) in
      A.induce task
    ) |> Array.concat_map ~f:ident
  in

  match procedure with
  | Test_set testset ->
    let results = Array.map full_models ~f:(fun (m,t) ->
        {
          model = m;
          induction_time = t;
          predicting_time = 0.;
          real_values = Data.get_targets testset;
          predictions = initialize_array (Array.length testset.Data.samples) [];
        })
    in
    Array.iter2_exn full_models results ~f:(fun (m,_) r -> update_result testset m r);
    results

  | CrossValidation (rounds,folds) ->
    let results = Array.map full_models ~f:(fun (m,t) ->
        {
          model = m;
          induction_time = t;
          predicting_time = 0.;
          real_values = Data.get_targets task.Task.data;
          predictions = initialize_array (Array.length task.Task.data.Data.samples) [];
        })
    in
    cross_validation rounds folds task algorithms results;
    results

  | RandomSplit (rounds,ratio) ->
    let results = Array.map full_models ~f:(fun (m,t) ->
        {
          model = m;
          induction_time = t;
          predicting_time = 0.;
          real_values = Data.get_targets task.Task.data;
          predictions = initialize_array (Array.length task.Task.data.Data.samples) [];
        })
    in
    random_split_validation rounds ratio task algorithms results;
    results
