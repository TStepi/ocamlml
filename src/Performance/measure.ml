open Utils
module D = Data

type performance =
  | Single of string*float
  | PerComponent of performance array array

type t = D.t list array -> D.t array -> performance



let per_component component_measures predictions real_values =
  PerComponent (Array.mapi component_measures ~f:(fun i measures ->
      Array.map measures ~f:(fun measure ->
          measure
            (Array.map predictions ~f:(fun lst -> List.map lst ~f:(fun p -> p.D.components.(i))))
            (Array.map real_values ~f:(fun r -> r.D.components.(i)))
        )))


let component_average component_measures predictions real_values =
  let performances = Array.mapi component_measures ~f:(fun i measure ->
      let m = measure
          (Array.map predictions ~f:(fun lst -> List.map lst ~f:(fun p -> p.D.components.(i))))
          (Array.map real_values ~f:(fun r -> r.D.components.(i)))
      in
      match m with
      | Single (_,v) -> v
      | PerComponent _ -> failwith "Component average doesn't work with this measure."
    ) in
  Single ("Component average", float_array_mean performances)


let relative_misses predictions real_values =
  let misses = ref 0 in
  let total = ref 0 in
  for i=0 to Array.length real_values - 1 do
    List.iter predictions.(i) ~f:(fun p ->
        match real_values.(i).D.value, p.D.value with
        | None, _ -> ()
        | _, None -> failwith "Missing prediction"
        | Some r, Some p ->
          incr total;
          if r <> p then incr misses
      )
  done;
  Single ("relative misses", (float !misses) /. (float !total))


let count reliability predictions real_values =
  let total = ref 0 in
  for i=0 to Array.length real_values -1 do
    List.iter predictions.(i) ~f:(fun p ->
        if p.D.reliability > reliability &&  not (Data.is_missing p) then incr total
      )
  done;
  Single (sprintf "Count rel > %.3f" reliability, float !total)


let mae reliability predictions real_values =
  let error = ref 0. in
  let total = ref 0 in
  for i=0 to Array.length real_values - 1 do
    List.iter predictions.(i) ~f:(fun p ->
        if p.D.reliability > reliability then
          match real_values.(i).D.value, p.D.value with
          | None, _ -> ()
          | _, None -> failwith "Missing prediction"
          | Some r, Some p ->
            incr total;
            let diff = r -. p in
            error := !error +. (Float.abs diff)
      )
  done;
  Single (sprintf "MAE rel > %.3f" reliability, !error /. (float !total))



let mse predictions real_values =
  let error = ref 0. in
  let total = ref 0 in
  for i=0 to Array.length real_values - 1 do
    List.iter predictions.(i) ~f:(fun p ->
        match real_values.(i).D.value, p.D.value with
        | None, _ -> ()
        | _, None -> failwith "Missing prediction"
        | Some r, Some p ->
          incr total;
          let diff = r -. p in
          error := !error +. diff*.diff
      )
  done;
  Single ("MSE", !error /. (float !total))



let mse_var predictions real_values =
  let variance = ref 0. in
  for i=0 to Array.length real_values - 1 do
    let preds = ref 0 in
    let sum = ref 0. in
    let sqsum = ref 0. in
    List.iter predictions.(i) ~f:(fun p ->
        incr preds;
        let v = match p.D.value with
          | None -> failwith "Missing prediction"
          | Some v -> v
        in
        sum := !sum +. v;
        sqsum := !sqsum +. v*.v
      );
    let preds = float !preds in
    let avg = !sum /. preds in
    variance := !variance +. !sqsum /. preds -. avg*.avg
  done;
  Single ("MSE variance", !variance /. (Array.length real_values |> float))



let mse_bias predictions real_values =
  let bias = ref 0. in
  for i=0 to Array.length real_values - 1 do
    match real_values.(i).D.value with
    | None -> ()
    | Some r -> begin
        let preds = ref 0 in
        let sum = ref 0. in
        List.iter predictions.(i) ~f:(fun p ->
            incr preds;
            match p.D.value with
            | None -> failwith "Missing prediction"
            | Some v -> sum := !sum +. v
          );
        let b = r -. !sum /. (float !preds) in
        bias := !bias +. b*.b
      end
  done;
  Single ("MSE bias", !bias /. (Array.length real_values |> float))



let rmse reliability predictions real_values =
  let error = ref 0. in
  let total = ref 0 in
  for i=0 to Array.length real_values - 1 do
    List.iter predictions.(i) ~f:(fun p ->
        if p.D.reliability > reliability then
          match real_values.(i).D.value, p.D.value with
          | None, _ -> ()
          | _, None -> failwith "Missing prediction"
          | Some r, Some p ->
            incr total;
            let diff = r -. p in
            error := !error +. diff*.diff
      )
  done;
  Single (sprintf "RMSE rel > %.3f" reliability, !error /. (float !total) |> sqrt)




let ad dist predictions real_values =
  let error = ref 0. in
  let total = ref 0 in
  for i=0 to Array.length real_values - 1 do
    List.iter predictions.(i) ~f:(fun p ->
        incr total;
        let diff = dist real_values.(i) p in
        error := !error +. diff
      )
  done;
  Single ("AD", !error /. (float !total))



let rasd dist predictions real_values =
  let error = ref 0. in
  let total = ref 0 in
  for i=0 to Array.length real_values - 1 do
    List.iter predictions.(i) ~f:(fun p ->
        incr total;
        let diff = dist real_values.(i) p in
        error := !error +. diff*.diff
      )
  done;
  Single ("RASD", !error /. (float !total) |> sqrt)



(* Classification *)
let confusion_matrix_raw class_names predictions real_values =
  let cnum = Array.length class_names in
  let matrix = Array.make_matrix cnum cnum 0 in
  let save_result solution prediction =
    match solution.D.value, prediction.D.value with
    | None, _ -> ()
    | _, None -> failwith "Missing prediction"
    | Some r, Some p ->
      let a = Int.of_float r in
      let b = Int.of_float p in
      matrix.(a).(b) <- matrix.(a).(b) +1
  in
  for i=0 to Array.length real_values - 1 do
    List.iter predictions.(i) ~f:(fun p ->
        save_result real_values.(i) p
      )
  done;
  matrix

let two_class_stats confusion_matrix =
  let n = Array.length confusion_matrix in
  let analyze i =
    let tp,fp,tn,fn = ref 0, ref 0, ref 0, ref 0 in
    for j=0 to n-1 do
      for k=0 to n-1 do
        if j=i then
          if k=i then tp := !tp+confusion_matrix.(j).(k) else fn := !fn+confusion_matrix.(j).(k)
        else
        if k=i then fp := !fp+confusion_matrix.(j).(k) else tn := !tn+confusion_matrix.(j).(k)
      done
    done;
    !tp,!fp,!tn,!fn
  in
  Array.init n analyze

let precision_fromtuple (tp,fp,tn,fn) =
  let denom = tp + fp  in
  if denom = 0 then 1. else
    (float tp ) /. (float denom)

let recall_fromtuple (tp,fp,tn,fn) =
  let denom = tp + fn in
  if denom = 0 then 1. else
    (float tp ) /. (float denom)

let f1_fromtuple (tp,fp,tn,fn) =
  let denom = 2*tp + fp + fn in
  if denom = 0 then 1. else
    (float (2*tp)) /. (float denom)


let precision names predictions real_values =
  let cm = confusion_matrix_raw names predictions real_values in
  let stats = two_class_stats cm in
  let weights = Array.map stats ~f:(fun (a,b,c,d) -> float (a+d)) in
  float_array_make_convex weights;
  let error = 1. -. Array.fold2_exn weights stats ~init:0. ~f:(fun total w tuple ->
      if w=0. then total else total +. w *. precision_fromtuple tuple
    ) in
  Single ("Precision", error)


let recall names predictions real_values =
  let cm = confusion_matrix_raw names predictions real_values in
  let stats = two_class_stats cm in
  let weights = Array.map stats ~f:(fun (a,b,c,d) -> float (a+d)) in
  float_array_make_convex weights;
  let error = 1. -. Array.fold2_exn weights stats ~init:0. ~f:(fun total w tuple ->
      if w=0. then total else total +. w *. recall_fromtuple tuple
    ) in
  Single ("Recall", error)



let f1 names predictions real_values =
  let cm = confusion_matrix_raw names predictions real_values in
  let stats = two_class_stats cm in
  let weights = Array.map stats ~f:(fun (a,b,c,d) -> float (a+d)) in
  float_array_make_convex weights;
  let error = 1. -. Array.fold2_exn weights stats ~init:0. ~f:(fun total w tuple ->
      if w=0. then total else total +. w *. f1_fromtuple tuple
    ) in
  Single ("F1", error)


let apply_threshold threshold predictions =
  Array.map predictions ~f:(fun lst ->
      List.map lst ~f:(fun p ->
          let new_v = match p.D.value with
            | None -> None
            | Some v -> if v > threshold then Some 1. else Some 0.
          in
          {D.value = new_v; D.components = [||]; D.reliability = p.D.reliability}
        ))


let binary_precision threshold predictions real_values =
  precision [|"Negative"; "Positive"|] (apply_threshold threshold predictions) real_values

let binary_recall threshold predictions real_values =
  recall [|"Negative"; "Positive"|] (apply_threshold threshold predictions) real_values

let binary_f1 threshold predictions real_values =
  f1 [|"Negative"; "Positive"|] (apply_threshold threshold predictions) real_values
