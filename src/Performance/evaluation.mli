open Utils

type result = {
    model : Model.t;
    induction_time : time;
    mutable predicting_time : time;
    real_values : Data.t array;
    predictions : Data.t list array;
  }


type t =
  | Test_set of Data.subset
  | CrossValidation of int*int
  | RandomSplit of int*subarray_size


val evaluate : t -> Task.t -> (module Algorithm.Modeler) array -> result array


(* val predetermined_validation : *)
(*   measures:Measure.t array -> *)
(*   out_distance:Distance.c -> *)
(*   trainset:Data.subset -> *)
(*   testset:Data.subset -> *)
(*   algorithms:(module Algorithm.Predictor) array -> *)
(*   result array *)


(* val random_split_validation : *)
(*   rounds:int -> *)
(*   percentage:float -> *)
(*   data:Data.subset -> *)
(*   out_distance:Distance.c -> *)
(*   measures:Measure.t array -> *)
(*   algorithms:(module Algorithm.Predictor) array -> *)
(*   result array *)


(* val cross_validation : *)
(*   rounds:int -> *)
(*   folds:int -> *)
(*   data:Data.subset -> *)
(*   out_distance:Distance.c -> *)
(*   measures:Measure.t array -> *)
(*   algorithms:(module Algorithm.Predictor) array -> *)
(*   result array *)
