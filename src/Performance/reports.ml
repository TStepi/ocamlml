open Utils


let init ~report_path ~data ~comments =
  let write_channel = Out_channel.create report_path in
  fprintf write_channel "Dataset: %s\n\n" data.Data.dataset.Data.name;
  fprintf write_channel "Instances: %d\n\n" (Array.length data.Data.samples);

  Data.attribute_structure_of_subset data
  |> Structure.to_string ~short:true
  |> fprintf write_channel "Attribute structure:\n%s\n\n";

  Data.target_structure_of_subset data
  |> Structure.to_string ~short:false
  |> fprintf write_channel "Target structure:\n%s\n\n";

  fprintf write_channel "Comments:\n%s\n" comments;
  Out_channel.close write_channel


let model_report ?(show_model=false) ~report_path ~measures ~result =
  let model = result.Evaluation.model in
  let write_channel = Out_channel.create ~append:true report_path in
  fprintf write_channel "\n\n\n%s\n\n" (String.make 100 '#');
  fprintf write_channel "Model description: %s\n" model.Model.description;
  if show_model then fprintf write_channel "Model:\n%s\n" (model.Model.to_string ());
  fprintf write_channel "\nInduction time: %.3f\n" result.Evaluation.induction_time;
  fprintf write_channel "Predicting time: %.3f\n\n" result.Evaluation.predicting_time;

  (* fprintf write_channel "Real values:\n%s\n" *)
  (*   (array_to_string ~f:Data.to_string result.Evaluation.real_values); *)
  (* fprintf write_channel "Predicted values:\n%s\n\n" *)
  (*   (array_to_string ~f:(List.to_string ~f:Data.to_string) result.Evaluation.predictions); *)

  match measures result.Evaluation.predictions result.Evaluation.real_values with
  | Measure.Single _ -> failwith "You should rethink the Measure module"
  | Measure.PerComponent performances_per_component -> begin
      Array.iteri performances_per_component ~f:(fun target performances ->
          fprintf write_channel "Target %d\n" target;
          Array.iter performances ~f:(fun p ->
              match p with
              | Measure.PerComponent _ -> failwith "You should rethink the Measure module"
              | Measure.Single (name,value) -> fprintf write_channel "%s: %.3f\n" name value
            );
          fprintf write_channel "\n"
        );
    end;

  Out_channel.close write_channel


(* let ranker_report ~report_path ~algorithm ~result ~time = *)
(*   let module A = (val algorithm : Algorithm.Ranker ) in *)
(*   let write_channel = Out_channel.create report_path ~append:true in *)
(*   fprintf write_channel "\n\n\n%s\n\n" (String.make 100 '#'); *)
(*   fprintf write_channel "Ranker: %s\n" A.description; *)
(*   fprintf write_channel "Log:\n%s\n\n" (A.log ()); *)
(*   fprintf write_channel "Time: %.3f\n" time; *)
(*   fprintf write_channel "Structure:\n%s" (Structure.to_string result); *)
(*   Out_channel.close write_channel *)
