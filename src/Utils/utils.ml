let time = Sys.time
include Core
include Floatarray


type time = float
type weight = float

let (%) f g x = f (g x)

let array_to_string ~f arr = Array.to_list arr |> List.to_string ~f:f

let rec concat_lists lst1 lst2 =
  match lst1 with
  | [] -> lst2
  | x::xs -> concat_lists xs (x::lst2)

(**Ratio of a circle's circumference to its diameter.*)
let pi = 2.*.Float.atan Float.infinity

(**Creates a string of n spaces*)
let pad n = String.make n ' '

(**Times the time needed to evaluate the expression*)
let timed expression reference =
  print_endline reference;
  let start_time = time () in
  let result = expression () in
  let end_time = time () in
  end_time -. start_time
  |> Float.to_string
  |> print_endline;
  result
(**String argument reference is printed with the time to the console*)

(**Takes a list of lists and creates a cartesian product of them*)
let rec cartesian_product_multiple lst =
  match lst with
  | [] -> []
  | [lst]  ->
    List.map lst ~f:(fun x -> [x])
  | first :: rest ->
    cartesian_product_multiple rest
    |> List.map ~f:(fun lst -> List.map first ~f:(fun x -> x :: lst))
    |> List.concat


(**Returns the index of a specified item in the array. Crashes if not found*)
let index_of_exn item lst =
  let rec find index item =
    if index +1 > Array.length lst then failwith "Cannot find the element in the array."
    else if lst.(index) = item then index
    else find (index+1) item
  in
  find 0 item

(**Extracts the n-th element out of a list. Returns a pair (remainder,element).*)
let rec split_exn sez n = match sez with
  | [] -> failwith "Locujes prazen/prekratek seznam"
  | x::xs when n = 0 -> (xs,x)
  | x::xs -> let (a,b) = split_exn xs (n-1) in (x::a,b)

(**Minimum of 3 floats*)
let min_3float a b c = Float.min a (Float.min b c)

(**Returns the closest float on interval [lower,upper].*)
let bounded_to ~lower ~upper a =
  Float.max lower @@ Float.min a upper


(**Finds elements in par that are not in sub. *)
let find_complement par sub =
  let rec find_complement par sub found =
    match par,sub with
    | [],_ -> found
    | _,[] -> concat_lists par found
    | (x::xs),(y::ys) ->
      if x=y then find_complement xs ys found
      else if x<y then find_complement xs sub (x::found)
      else find_complement par ys found
  in
  find_complement par sub []


(**Similar to random subarray but instead of a chance you give the number of selected elements.
   No repetitions.*)

type subarray_size =
  | All
  | Fixed of int
  | Share of float
  | Log
  | Sqrt

let random_elements_slow number arr =
  let module S = Set.Make(Int) in
  let size = Array.length arr in
  let chosen = ref S.empty in
  while Set.length !chosen < number do
    chosen := S.add !chosen (Random.int size)
  done;
  Set.to_array !chosen |> Array.map ~f:(fun i -> arr.(i))

let random_subarray size arr =
  if size = All then arr else begin
    let total = Array.length arr in
    let number = match size with
      | All -> failwith "This was checked before to not be true"
      | Fixed a -> a
      | Share a -> float total *. a |> Float.round_up |> Int.of_float
      | Log -> Float.log (float total) |> Float.round_up |> Int.of_float
      | Sqrt -> Float.sqrt (float total) |> Float.round_up |> Int.of_float
    in
    if 2*number < total then
      random_elements_slow number arr
    else
      random_elements_slow (total-number) arr
      |> Array.to_list
      |> find_complement (Array.to_list arr)
      |> List.to_array
  end


(* quickselect functions *)

let partition key arr left right pivot =
  let pv = arr.(pivot) in
  Array.swap arr pivot right;
  let ind = ref left in
  for i=left to right -1 do
    if key arr.(i) pv = (-1) then (Array.swap arr !ind i; incr ind)
  done;
  Array.swap arr right !ind;
  !ind


let rec select' key arr left right k =
  if left<right then
    let p_ind = (left+right)/2 in
    let p_new = partition key arr left right p_ind in
    select' key arr left (p_new-1) k;
    if p_new < k-1 then select' key arr (p_new+1) right k

let partialsort arr k key =
  select' key arr 0 (Array.length arr -1) k


let float_list_sum lst =
  let rec float_list_sum total lst =
    match lst with
    | [] -> total
    | x::xs -> float_list_sum (total+.x) xs
  in
  float_list_sum 0. lst
