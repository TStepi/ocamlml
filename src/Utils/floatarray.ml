module C = Core

let initialize_array = Array.make

let initialize_float_array n = Array.make n 0.

let float_array_min lst =
  let t = ref C.Float.infinity in
  for i=0 to Array.length lst -1 do
    if C.Float.compare !t lst.(i) = 1 then t := lst.(i)
  done;
  !t

let float_array_max lst =
  let t = ref C.Float.neg_infinity in
  for i=0 to Array.length lst -1 do
    if C.Float.compare !t lst.(i) = -1 then t := lst.(i)
  done;
  !t

(* vsota float seznama *)
let float_array_sum xs =
  let t = ref 0.0 in
  for i=0 to Array.length xs-1 do
    t := !t +. xs.(i)
  done;
  !t

let float_array_mean lst = (float_array_sum lst) /. (float @@ Array.length lst)

let float_array_variance lst =
  let n = Array.length lst in
  let sum = ref 0. in
  let sumsq = ref 0. in
  for i=0 to n-1 do
    sum := !sum +. lst.(i);
    sumsq := !sumsq +. lst.(i)*.lst.(i)
  done;
  let n = float n in
  (!sumsq -. !sum *. !sum /. n) /. n

let float_array_scale lst factor =
  for i=0 to Array.length lst - 1 do
    lst.(i) <- lst.(i)*.factor
  done

(* naredi konveksne utezi iz float seznama *)
let float_array_make_convex lst =
  let total = float_array_sum lst in
  if total = 0. then ()
  else float_array_scale lst (1./.total)

(* normira pozitiven float seznam *)
let float_array_normalize lst =
  let maks = float_array_max lst in
  if maks = 0. then ()
  else float_array_scale lst (1./.maks)

let float_array_span lst =
  (float_array_max lst) -. (float_array_min lst)

let float_array_ranked lst =
  let sorted = C.Array.sorted_copy lst ~cmp:C.Float.compare in
  let table = C.Hashtbl.create ~hashable:C.Float.hashable () in
  let start = ref 0 in
  let stop = ref 0 in
  let n = Array.length lst in
  while !start < n do
    stop := !start;
    while (!stop < (n-1) && sorted.(!start)=sorted.(!stop+1)) do stop:=!stop+1 done;
    C.Hashtbl.add_exn table sorted.(!start) (float (!start + !stop +2) /. 2.);
    start := !stop + 1
  done;
  C.Array.map lst ~f:(C.Hashtbl.find_exn table)


let float_array_inf_scale arr =
  let has_inf = ref false in
  for i=0 to Array.length arr -1 do
    if not !has_inf && C.Float.is_inf arr.(i) then has_inf := true
  done;
  if !has_inf then begin
    for i=0 to Array.length arr -1 do
      arr.(i) <- if C.Float.is_inf arr.(i) then 1. else 0.
    done
  end


let float_array_max_ind arr =
  let ind = ref 0 in
  for i=0 to Array.length arr -1 do
    if C.Float.compare arr.(!ind) arr.(i) = -1 then ind := i
  done;
  !ind

let float_array_min_ind arr =
  let ind = ref 0 in
  for i=0 to Array.length arr -1 do
    if C.Float.compare arr.(!ind) arr.(i) = 1 then ind := i
  done;
  !ind

let float_array_translate arr x =
  for i=0 to Array.length arr -1 do
    arr.(i) <- arr.(i) +. x
  done
