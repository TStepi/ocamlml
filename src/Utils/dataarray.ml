module C = Core.Std
module D = Data

let data_array_min lst =
  let t = ref C.Float.infinity in
  for i=0 to Array.length lst -1 do
    if C.Float.compare !t lst.(i).D.value = 1 then t := lst.(i).D.value
  done;
  !t

let data_array_max lst =
  let t = ref C.Float.neg_infinity in
  for i=0 to Array.length lst -1 do
    if C.Float.compare !t lst.(i).D.value = -1 then t := lst.(i).D.value
  done;
  !t

(* vsota data seznama *)
let data_array_sum xs =
  let t = ref 0.0 in
  for i=0 to Array.length xs-1 do
    t := !t +. xs.(i).D.value
  done;
  !t

let data_array_mean lst = (data_array_sum lst) /. (float @@ Array.length lst)

let data_array_variance lst =
  let n = Array.length lst in
  let sum = ref 0. in
  let sumsq = ref 0. in
  for i=0 to n-1 do
    let v = lst.(i).D.value in
    sum := !sum +. v;
    sumsq := !sumsq +. v*.v
  done;
  let n = float n in
  (!sumsq -. !sum *. !sum /. n) /. n

let data_array_span lst =
  (data_array_max lst) -. (data_array_min lst)

let data_array_ranked lst =
  let sorted = C.Array.sorted_copy lst ~cmp:D.compare in
  let table = C.Hashtbl.create ~hashable:C.Float.hashable () in
  let start = ref 0 in
  let stop = ref 0 in
  let n = Array.length lst in
  while !start < n do
    stop := !start;
    while (!stop < (n-1) && sorted.(!start).D.value = sorted.(!stop+1).D.value) do stop:=!stop+1 done;
    C.Hashtbl.add_exn table sorted.(!start).D.value (float (!start + !stop +2) /. 2.);
    start := !stop + 1
  done;
  C.Array.map lst ~f:(fun d -> C.Hashtbl.find_exn table d.D.value)


let data_array_max_ind arr =
  let ind = ref 0 in
  for i=0 to Array.length arr -1 do
    if C.Float.compare arr.(!ind).D.value arr.(i).D.value = -1 then ind := i
  done;
  !ind

let data_array_min_ind arr =
  let ind = ref 0 in
  for i=0 to Array.length arr -1 do
    if C.Float.compare arr.(!ind).D.value arr.(i).D.value = 1 then ind := i
  done;
  !ind
