open Utils

type t =
  | Real
  | Integer
  | Binary
  | Nominal of string array
  | Tuple of t array
  | Set of t
  | Sequence of t
  | Series of int*t

let to_string ?(short=false) s =
  let acc = ref [] in
  let rec to_string n s =
    match s with
    | Nominal values ->
      if short then
        acc := (sprintf "%sNominal - %d values" (pad n) (Array.length values)) :: !acc
      else
        let descr = Array.to_list values
                    |> List.to_string ~f:ident in
        acc := (sprintf "%sNominal %s" (pad n) descr) :: !acc
    | Real ->
      acc := (sprintf "%sReal" (pad n)) :: !acc
    | Binary ->
      acc := (sprintf "%sBinary" (pad n)) :: !acc
    | Integer ->
      acc := (sprintf "%sInt" (pad n)) :: !acc
    | Set s ->
      acc := (sprintf "%sSet" (pad n)) :: !acc; to_string (n+4) s
    | Sequence s ->
      acc := (sprintf "%sSequence" (pad n)) :: !acc; to_string (n+4) s
    | Series (len,s) ->
      acc := (sprintf "%sSeries len=%d" (pad n) len) :: !acc; to_string (n+4) s
    | Tuple lst ->
      if short then
        acc := (sprintf "%sTuple   dim = %d" (pad n) (Array.length lst)) :: !acc
      else begin
        acc := (sprintf "%sTuple\n" (pad n)) :: !acc;
        Array.iter ~f:(to_string (n+4)) lst
      end
  in
  to_string 0 s;
  let temp = List.rev !acc in
  String.concat ~sep:"\n" temp
