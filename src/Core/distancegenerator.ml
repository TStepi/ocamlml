open Utils
module D = Distance

type t = Statistic.t -> Distance.c
type pool = Statistic.t -> Distance.c array

type powermean_parameters =
  {
    p : float;
    ow : (int -> int -> float);
    name : string;
  }

type tuple_tag =
  | Powermean of powermean_parameters
  | Average
  | Euclidean
  | Median
  | Maximum

type sequence_tag =
  | Warping of int
  | Edit

type series_tag =
  | WarpingS of int
  | EditS
  | Qualitative
  | Pearson
  | Spearman
  | AverageS
  | EuclideanS
  | MedianS
  | MaximumS
  | PowermeanS of powermean_parameters

type set_tag =
  | Jaccard
  | Hausdorff
  | SingleLink
  | CompleteLink
  | AverageLink


let span numeric_stats =
  let module S = Statistic in
  let stats = numeric_stats in
  let var = (stats.S.sumsq -. stats.S.sum *. stats.S.sum /. stats.S.count) /. stats.S.count in
  2. *. (Float.max Float.epsilon_float (Float.sqrt var))


let rec generate_distance_pool ~tuples ~series ~sequences ~sets ~statistic =
  match statistic with
  | Statistic.Nominal _ ->
    [|D.Delta|]

  | Statistic.Numeric stats ->
    [|D.Ramp (span stats)|]

  | Statistic.Sequence s ->
    let ds = generate_distance_pool ~tuples ~series ~sequences ~sets ~statistic:s in
    Array.map sequences ~f:(fun seq ->
        match seq with
        | Warping w -> Array.map ds ~f:(fun d -> D.Warping (w,d))
        | Edit -> Array.map ds ~f:(fun x -> D.Edit x)
      )
    |> Array.concat % Array.to_list

  | Statistic.Set s ->
    let ds = generate_distance_pool ~tuples ~sequences ~series ~sets ~statistic:s in
    Array.map sets ~f:(fun set ->
        match set with
        | SingleLink -> Array.map ds ~f:(fun d -> D.SingleLink d)
        | CompleteLink -> Array.map ds ~f:(fun d -> D.CompleteLink d )
        | AverageLink -> Array.map ds ~f:(fun d -> D.AverageLink d)
        | Hausdorff -> Array.map ds ~f:(fun d -> D.Hausdorff d)
        | Jaccard -> [|D.Jaccard|]
      )
    |> Array.concat % Array.to_list

  | Statistic.Series (n, Statistic.Numeric stats) ->
    let span = span stats in
    Array.map series ~f:(fun seria ->
        match seria with
        | WarpingS w -> D.Warping (w,D.Ramp span)
        | EditS -> D.Edit (D.Ramp span)
        | AverageS -> D.Average (initialize_array n 1.,initialize_array n (D.Ramp span))
        | EuclideanS -> D.Euclidean (initialize_array n 1.,initialize_array n (D.Ramp span))
        | MedianS -> D.Median (initialize_array n 1.,initialize_array n (D.Ramp span))
        | MaximumS -> D.Maximum (initialize_array n 1.,initialize_array n (D.Ramp span))
        | PowermeanS p -> D.Powermean (p.p,Array.init n (p.ow n),initialize_array n 1.,initialize_array n (D.Ramp span))
        | Spearman -> D.Spearman
        | Pearson -> D.Pearson
        | Qualitative -> D.Qualitative 0.2
      )

  | Statistic.Series (n,s) ->
    let ds = generate_distance_pool ~tuples ~sequences ~series ~sets ~statistic:s in
    Array.map series ~f:(fun seria ->
        match seria with
        | WarpingS w -> Array.map ds ~f:(fun d -> D.Warping (w,d))
        | EditS -> Array.map ds ~f:(fun d -> D.Edit d)
        | AverageS -> Array.map ds ~f:(fun d ->
            D.Average (initialize_array n 1., initialize_array n d)
          )
        | MedianS -> Array.map ds ~f:(fun d ->
            D.Median (initialize_array n 1., initialize_array n d)
          )
        | MaximumS -> Array.map ds ~f:(fun d ->
            D.Maximum (initialize_array n 1., initialize_array n d)
          )
        | EuclideanS -> Array.map ds ~f:(fun d ->
            D.Euclidean (initialize_array n 1., initialize_array n d)
          )
        | PowermeanS p -> Array.map ds ~f:(fun d ->
            D.Powermean (p.p,Array.init n (p.ow n),initialize_array n 1., initialize_array n d)
          )
        | _ -> [||]
      )
    |> Array.concat % Array.to_list

  | Statistic.Tuple (ranks,lst) ->
    let components =
      let lst = Array.to_list lst in
      List.map lst (fun s -> generate_distance_pool ~tuples ~sequences ~series ~sets ~statistic:s)
      |> List.map ~f:Array.to_list
      |> cartesian_product_multiple
      |> List.map ~f:List.to_array
    in
    let n = Array.length lst in
    List.map components ~f:(fun dims ->
        Array.map tuples ~f:(fun tag ->
            match tag with
            | Average -> D.Average (ranks,dims)
            | Euclidean -> D.Euclidean (ranks,dims)
            | Median -> D.Median (ranks,dims)
            | Maximum -> D.Maximum (ranks,dims)
            | Powermean params ->
              D.Powermean (params.p,(Array.init n (params.ow n)),ranks,dims)
          )
      )
    |> Array.concat



let rec generate_distance ~tuple ~seria ~sequence ~set ~statistic =
  match statistic with
  | Statistic.Nominal _ -> D.Delta

  | Statistic.Numeric stats ->
    D.Ramp (span stats)

  | Statistic.Sequence s ->
    let d = generate_distance ~tuple ~seria ~sequence ~set ~statistic:s in
    begin match sequence with
      | Warping w -> D.Warping (w,d)
      | Edit -> D.Edit d
    end

  | Statistic.Set s ->
    let d = generate_distance ~tuple ~seria ~sequence ~set ~statistic:s in
    begin match set with
      | SingleLink -> D.SingleLink d
      | CompleteLink -> D.CompleteLink d
      | AverageLink -> D.AverageLink d
      | Hausdorff -> D.Hausdorff d
      | Jaccard -> D.Jaccard
    end

  | Statistic.Series (n,Statistic.Numeric stats) ->
    let span = span stats in
    begin match seria with
      | WarpingS w -> D.Warping (w,D.Ramp span)
      | EditS -> D.Edit (D.Ramp span)
      | Spearman -> D.Spearman
      | Pearson -> D.Pearson
      | Qualitative -> D.Qualitative 0.2
      | AverageS -> D.Average (initialize_array n 1., initialize_array n (D.Ramp span))
      | EuclideanS -> D.Euclidean (initialize_array n 1., initialize_array n (D.Ramp span))
      | MedianS -> D.Median (initialize_array n 1., initialize_array n (D.Ramp span))
      | MaximumS -> D.Maximum (initialize_array n 1., initialize_array n (D.Ramp span))
      | PowermeanS p -> D.Powermean (p.p,Array.init n (p.ow n),initialize_array n 1., initialize_array n (D.Ramp span))
    end

  | Statistic.Series (n,s) ->
    let d = generate_distance ~tuple ~seria ~sequence ~set ~statistic:s in
    begin match seria with
      | WarpingS w -> D.Warping (w,d)
      | EditS -> D.Edit d
      | AverageS -> D.Average (initialize_array n 1., initialize_array n d)
      | EuclideanS -> D.Euclidean (initialize_array n 1., initialize_array n d)
      | MedianS -> D.Median (initialize_array n 1., initialize_array n d)
      | MaximumS -> D.Maximum (initialize_array n 1., initialize_array n d)
      | PowermeanS p -> D.Powermean (p.p, Array.init n (p.ow n),initialize_array n 1., initialize_array n d)
      | _ -> failwith "Not a real series"
    end

  | Statistic.Tuple (ranks,lst) ->
    let dims = Array.map lst ~f:(fun s -> generate_distance ~tuple ~seria ~sequence ~set ~statistic:s) in
    let n = Array.length lst in
    begin match tuple with
      | Average -> D.Average (ranks,dims)
      | Euclidean -> D.Euclidean (ranks,dims)
      | Median -> D.Median (ranks,dims)
      | Maximum -> D.Maximum (ranks,dims)
      | Powermean params ->
        D.Powermean (params.p,(Array.init n (params.ow n)), ranks, dims)
    end


let construct_pool ~tuples ~series ~sequences ~sets =
  fun statistic -> generate_distance_pool ~tuples ~series ~sequences ~sets ~statistic

let construct ~tuple ~seria ~sequence ~set =
  fun statistic -> generate_distance ~tuple ~seria ~sequence ~set ~statistic




(* some owa weights generators *)
let triangle_ow n i =
  if n=1 || n=2 then 1. else
    let n,i = float (n-1), float i in
    n/.2. -. Float.abs (n/.2. -. i)

let trimmed_ow n i =
  if Int.between i ~low:(n/4 + 1) ~high:((3*n-1)/4 - 1) then 4.
  else if i=n/4 || i=(3*n-1)/4 then float (4 - n mod 4)
  else 0.

let step_ow n i =
  if i > (n-1)/2 then 2.
  else if n mod 2 = 1 && i = (n-1)/2 then 1.
  else 0.

let gauss_ow n i =
  let n,i = float (n-1), float i in
  let b = n/.2. in
  let c = Float.max 1. (n**2./.13.) in
  exp @@ Float.neg @@ (i -. b) ** 2. /. c

let sigmoid_ow n i =
  let n,i = float (n-1), float i in
  let diff =  10. *. (1./.2. -. i/.n) in
  1. /. (1. +. exp diff)

let linear_ow n = float

let equal_ow n i = 1.

let maximum_ow n i = if i=n-1 then 1. else 0.

let median_ow n i = if i=n/2 || i=(n-1)/2 then 1. else 0.

(* Standard 10 aggregation testing setup*)
let average = { p = 1.; name = "average"; ow = equal_ow }
let euclidean = { p = 2.; name = "euclidean"; ow = equal_ow }
let maximum = { p = 1.; name = "maximum"; ow = maximum_ow }
let median = { p = 1.; name = "median"; ow = median_ow }
let linear = { p = 1.; name = "linear"; ow = linear_ow }
let sigmoid = { p = 1.; name = "sigmoid"; ow = sigmoid_ow }
let trimmed = { p = 1.; name = "trimmed"; ow = trimmed_ow }
let step = { p = 1.; name = "step"; ow = step_ow }
let gauss = { p = 1.; name = "gauss"; ow = gauss_ow }
let triangle = { p = 1.; name = "triangle"; ow = triangle_ow }
