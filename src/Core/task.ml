type t = {
  data : Data.subset;
  target_distance : Distance.c;
  main_measure : Measure.t
}
