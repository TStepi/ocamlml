open Utils

type t = {
  value: float option;
  components: t array;
  reliability: float

}

let compare d1 d2 =
  match d1.value, d2.value with
  | None, None -> 0
  | None, _ -> -1
  | _,None -> 1
  | Some x, Some y ->
    if x < y then -1
    else if x > y then 1
    else Int.compare (Array.length d1.components) (Array.length d2.components)

(* let to_string d = *)
(*   let acc = ref [] in *)
(*   let rec to_string n d = *)
(*     let repr = match d.value with *)
(*       | None -> "?" *)
(*       | Some x -> sprintf "%.3f" x *)
(*     in *)
(*     acc := (sprintf "%s%s" (pad n) repr) :: !acc; *)
(*     Array.iter d.components ~f:(to_string (n+4)) *)
(*   in *)
(*   to_string 0 d; *)
(*   String.concat ~sep:"\n" (List.rev !acc) *)


let rec to_string d =
  if Array.length d.components = 0 then begin
    match d.value with
    | Some v -> sprintf "%.3f (%.3f)" v d.reliability
    | None -> "?"
  end else begin
    let components =
      Array.to_list d.components
      |> List.map ~f:to_string
      |> String.concat ~sep:", "
    in
    match d.value with
    | None -> "?"
    | Some 1. -> sprintf "(%s)" components
    | Some 2. -> sprintf "{%s}" components
    | Some _ -> sprintf "[%s]" components
  end



let is_missing dat = Option.is_none dat.value
let missing = {value = None; components = [||]; reliability = 1.}

let mean_reliability arr =
  let n = Array.length arr in
  let r = ref 0. in
  for i=0 to n-1 do
    r := !r +. arr.(i).reliability
  done;
  !r /. (float n)

let nominal ?(reliability=1.) i =
  {value = Some (float i); components = [||]; reliability}

let binary ?(reliability=1.) v =
  {value = Some (if v then 1. else 0.); components = [||]; reliability}

let integer = nominal
let real ?(reliability=1.) r = {value = Some r; components = [||]; reliability}

let tuple arr = { value = Some 1.; components = arr; reliability = mean_reliability arr}
let set arr = { value = Some 2.; components = arr; reliability = mean_reliability arr}
let series arr = { value = Some 3.; components = arr; reliability = mean_reliability arr}
let sequence arr = {value = Some 4.; components = arr; reliability = mean_reliability arr}


let rec has_structure struc dat =
  match dat.value with
  | None -> true
  | Some value ->
    begin match struc with
      | Structure.Nominal names ->
        Array.length dat.components = 0 &&
        value < (float (Array.length names)) && 0. <= value

      | Structure.Binary ->
        value < 1. && 0. <= value

      | Structure.Real ->
        Array.length dat.components = 0

      | Structure.Integer  ->
        Array.length dat.components = 0 &&
        value = Float.round_nearest value

      | Structure.Set s ->
        value = 2. &&
        Array.for_all dat.components ~f:(has_structure s)

      | Structure.Sequence s ->
        value = 4. &&
        Array.for_all dat.components ~f:(has_structure s)

      | Structure.Series (n,s) ->
        value = 3. &&
        Array.for_all dat.components ~f:(has_structure s) &&
        Array.length dat.components = n

      | Structure.Tuple ss ->
        value = 1. &&
        Array.for_all2_exn ss dat.components ~f:has_structure
    end


type dataset = {
  name : string;
  structures : Structure.t array;
  values : t array array;
  variable_names : string array;
  size : int
}


type subset = {
  samples : int array;
  attributes : int array;
  importances : float array;
  targets : int array;
  dataset : dataset
}


let create_root_subset ~dataset ~disabled ~targets =
  (* quick check of settings' sanity *)
  Array.iter (Array.concat [disabled;targets]) ~f:(fun i ->
      if i<0 || i>= (Array.length dataset.structures) then print_endline "Check variable settings!\n"
    );

  let attributes =
    Array.init (Array.length dataset.structures) ~f:ident
    |> Array.filter ~f:(fun i ->
        not (Array.mem disabled i Int.equal || Array.mem targets i Int.equal)
      )
  in

  {
    samples = Array.init (Array.length dataset.values) ~f:ident;
    attributes;
    importances = initialize_array (Array.length attributes) 1.;
    targets;
    dataset;
  }


let get_attributes subset =
  Array.sort subset.samples ~cmp:Int.ascending;
  Array.map subset.samples ~f:(fun i ->
      tuple @@ Array.map subset.attributes ~f:(fun j ->
          subset.dataset.values.(i).(j)
        ))

let get_attribute subset index =
  tuple @@ Array.map subset.attributes ~f:(fun j ->
      subset.dataset.values.(index).(j)
    )

let get_targets subset =
  Array.sort subset.samples ~cmp:Int.ascending;
  Array.map subset.samples ~f:(fun i ->
      tuple @@ Array.map subset.targets ~f:(fun j ->
          subset.dataset.values.(i).(j)
        ))

let get_target subset index =
  tuple @@ Array.map subset.targets ~f:(fun j ->
      subset.dataset.values.(index).(j)
    )


let get_datum ~subset ~example ~component =
  subset.dataset.values.(subset.samples.(example)).(component)


let subset_complement parent subset =
  let sorted_parent = Array.sorted_copy ~cmp:Int.ascending parent.samples
                      |> Array.to_list in
  let sorted_subset = Array.sorted_copy ~cmp:Int.ascending subset.samples
                      |> Array.to_list in

  let complement = find_complement sorted_parent sorted_subset
                   |> List.to_array in
  {
    samples = complement;
    attributes = subset.attributes;
    importances = subset.importances;
    targets = subset.targets;
    dataset = subset.dataset
  }


let fold_subset ~subset ~folds ~fold =
  {
    samples = Array.filteri ~f:(fun i _ -> i mod folds = fold-1) subset.samples;
    attributes = subset.attributes;
    importances = subset.importances;
    targets = subset.targets;
    dataset = subset.dataset
  }


let bootstrap_subset subset =
  let n = Array.length subset.samples in
  let random_ind () = Random.int n in
  {
    samples = Array.init n ~f:(fun _ -> subset.samples.(random_ind ()));
    attributes = subset.attributes;
    importances = subset.importances;
    targets = subset.targets;
    dataset = subset.dataset
  }


let subspace_subset subspaces subset =
  let indices = Array.length subset.attributes
                |> Array.init ~f:ident
                |> random_subarray subspaces in
  {
    samples = subset.samples;
    attributes = Array.map indices ~f:(fun i -> subset.attributes.(i));
    importances = Array.map indices ~f:(fun i -> subset.importances.(i));
    targets = subset.targets;
    dataset = subset.dataset
  }


let random_subset size subset =
  {
    samples = random_subarray size subset.samples;
    attributes = subset.attributes;
    importances = subset.importances;
    targets = subset.targets;
    dataset = subset.dataset
  }


let attribute_structure_of_subset subset =
  let attrs = Array.map subset.attributes ~f:(fun i -> subset.dataset.structures.(i)) in
  Structure.Tuple  attrs


let target_structure_of_subset subset =
  let trgts = Array.map subset.targets ~f:(fun i -> subset.dataset.structures.(i)) in
  Structure.Tuple trgts



(* for saving subset to .arff files *)
let struct_descr struc =
  match struc with
  | Structure.Real -> "real"
  | Structure.Integer -> "integer"
  | Structure.Nominal values -> sprintf "{%s}" (String.concat ~sep:", " (Array.to_list values))
  | _ -> failwith "Not an .arff type"

let value_descr struc value =
  match value.value with
  | None -> "?"
  | Some value ->
    begin match struc with
      | (Structure.Real  | Structure.Integer ) -> Float.to_string value
      | Structure.Nominal values -> values.(Int.of_float value)
      | _ -> failwith "Not an .arff type"
    end

let subset_to_arff subset arff =
  let write_channel = Out_channel.create arff in
  let variables = Array.concat [subset.attributes; subset.targets] in

  fprintf write_channel "@relation %s\n" subset.dataset.name;
  Array.iter variables ~f:(fun a ->
      fprintf write_channel "@attribute %s %s\n"
        subset.dataset.variable_names.(a)
        (struct_descr subset.dataset.structures.(a))
    );

  fprintf write_channel "@data\n";
  Array.iter subset.samples ~f:(fun a ->
      let values = Array.map variables ~f:(fun v ->
          let struc = subset.dataset.structures.(v) in
          let value = subset.dataset.values.(a).(v) in
          value_descr struc value )
                   |> Array.to_list  in
      fprintf write_channel "%s\n" (String.concat values ~sep:",")
    );

  Out_channel.close write_channel


