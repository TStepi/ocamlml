open Utils

(* statistics used for numeric data *)
type numeric_stats = {
  mutable sum : float;
  mutable sumsq : float;
  mutable count : float;
}

(* Type representing statistics of data of different structures *)
type t =
  | Numeric of numeric_stats
  | Nominal of float array
  | Tuple of (float array)*(t array)
  | Set of t
  | Sequence of t
  | Series of int*t

val to_string : ?short:bool -> t -> string

val subset_target_stats : Data.subset -> t
val subset_attribute_stats : Data.subset -> t

val empty_copy : t -> t
val copy : t -> t

val variance : numeric_stats -> float
val l_variance : Distance.t -> Data.t array -> Data.t -> float
