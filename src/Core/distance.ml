open Utils

type t = Data.t -> Data.t -> float

(*type c = <
  calculate : t;
  update : Structure.t -> unit;
  to_string : ?indent:int -> ?short:bool -> unit -> string;
  get_components : c array
>*)

type c =
  | Delta
  | Ramp of float
  | Average of (float array)*(c array)
  | Euclidean of (float array)*(c array)
  | Maximum of (float array)*(c array)
  | Median of (float array)*(c array)
  | Powermean of float*(float array)*(float array)*(c array)
  | AverageLink of c
  | SingleLink of c
  | CompleteLink of c
  | Hausdorff of c
  | Jaccard
  | Qualitative of float
  | Spearman
  | Pearson
  | Edit of c
  | Warping of int*c

let dummy _ _ = 1.


let rec interpret (descr : c) : t =
  match descr with
  | Delta -> Delta.calculate
  | Ramp b -> Ramp.calculate b
  | Average (ws,ds) -> Average.calculate ws (float_array_sum ws) (Array.map interpret ds)
  | Euclidean (ws,ds) -> Euclidean.calculate ws (float_array_sum ws) (Array.map interpret ds)
  | Maximum (ws,ds) -> Maximum.calculate ws (Array.map interpret ds)
  | Median (ws,ds) -> Median.calculate ws (Array.map interpret ds)
  | Powermean (p,ows,dws,ds) -> Powermean.calculate p ows dws (Array.map interpret ds)
  | AverageLink d -> Link.average (interpret d)
  | SingleLink d -> Link.single (interpret d)
  | CompleteLink d -> Link.complete (interpret d)
  | Hausdorff d -> Hausdorff.calculate (interpret d)
  | Jaccard -> Jaccard.calculate
  | Qualitative tol -> Qualitative.calculate tol
  | Spearman -> Spearman.calculate
  | Pearson -> Pearson.calculate
  | Edit d -> Edit.calculate (interpret d)
  | Warping (w,d) -> Warping.calculate w (interpret d)
  (*| _ -> dummy*)


let rec to_string ?(indent=0) ?(short=false) d =
  match d with
  | Delta -> Delta.to_string indent
  | Ramp b -> Ramp.to_string indent short b
  | Average (ws,ds) -> Average.to_string indent short (Array.map ds ~f:(to_string ~indent ~short) |> Array.to_list) ws
  | Euclidean (ws,ds) -> Euclidean.to_string indent short (Array.map ds ~f:(to_string ~indent ~short) |> Array.to_list) ws
  | Maximum (ws,ds) -> Maximum.to_string indent short (Array.map ds ~f:(to_string ~indent ~short) |> Array.to_list) ws
  | Median (ws,ds) -> Median.to_string indent short (Array.map ds ~f:(to_string ~indent ~short) |> Array.to_list) ws
  | Powermean (p,ows,dws,ds) -> Powermean.to_string indent short (Array.map ds ~f:(to_string ~indent ~short) |> Array.to_list) p ows dws
  | AverageLink d -> Link.average_string indent short (to_string ~indent:(indent+4) ~short d)
  | SingleLink d -> Link.single_string indent short (to_string ~indent:(indent+4) ~short d)
  | CompleteLink d -> Link.complete_string indent short (to_string ~indent:(indent+4) ~short d)
  | Hausdorff d -> Hausdorff.to_string indent short (to_string ~indent:(indent+4) ~short d)
  | Jaccard -> Jaccard.to_string indent
  | Qualitative tol -> Qualitative.to_string indent
  | Spearman -> Spearman.to_string indent
  | Pearson -> Pearson.to_string indent
  | Edit d -> Edit.to_string indent short (to_string ~indent:(indent+4) ~short d)
  | Warping (w,d) -> Warping.to_string w indent short (to_string ~indent:(indent+4) ~short d)


(* let rec update struc dist = *)
(*   match struc,dist with *)
(*   | _,Delta -> Delta *)
(*   | (Structure.Real (_,_,s) | Structure.Integer (_,_,s)),Ramp _ -> Ramp s *)
(*   | Structure.Tuple (ws,ss), Average (_,ds) -> Average (ws,(Array.map2_exn ss ds ~f:update)) *)
(*   | Structure.Tuple (ws,ss), Euclidean (_,ds) -> Euclidean (ws,(Array.map2_exn ss ds ~f:update)) *)
(*   | Structure.Tuple (ws,ss), Maximum (_,ds) -> Maximum (ws,(Array.map2_exn ss ds ~f:update)) *)
(*   | Structure.Tuple (ws,ss), Median (_,ds) -> Median (ws,(Array.map2_exn ss ds ~f:update)) *)
(*   | Structure.Tuple (ws,ss), Powermean (p,ows,_,ds) -> Powermean (p,ows,ws, Array.map2_exn ss ds ~f:update) *)
(*   | Structure.Set s, AverageLink d -> AverageLink (update s d) *)
(*   | Structure.Set s, CompleteLink d -> CompleteLink (update s d) *)
(*   | Structure.Set s, SingleLink d -> SingleLink (update s d) *)
(*   | Structure.Set s, Hausdorff d -> Hausdorff (update s d) *)
(*   | _, Jaccard -> Jaccard *)
(*   | Structure.Sequence s, Edit d -> Edit (update s d) *)
(*   | Structure.Sequence s, Warping (w,d) -> Warping (w,update s d) *)
(*   | _, Spearman -> Spearman *)
(*   | _, Pearson -> Pearson *)
(*   | Structure.Series (_,Structure.Real (_,_,s)), Qualitative _ -> Qualitative (s*.0.1) *)
(*   | Structure.Series (_,s), Edit d -> Edit (update s d) *)
(*   | Structure.Series (_,s), Hausdorff d -> Hausdorff (update s d) *)
(*   | Structure.Series (_,s), Average (ws,ds) -> Average (ws,Array.map ds ~f:(update s)) *)
(*   | Structure.Series (_,s), Euclidean (ws,ds) -> Euclidean (ws,Array.map ds ~f:(update s)) *)
(*   | Structure.Series (_,s), Maximum (ws,ds) -> Maximum (ws,Array.map ds ~f:(update s)) *)
(*   | Structure.Series (_,s), Median (ws,ds) -> Median (ws,Array.map ds ~f:(update s)) *)
(*   | Structure.Series (_,s), Powermean (p,ows,dws,ds) -> Powermean (p,ows,dws,Array.map ds ~f:(update s)) *)
(*   | s,d -> failwith @@ sprintf "No update method for %s with %s" (to_string d) (Structure.to_string s) *)


let get_components dist =
  match dist with
  | (Delta | Ramp _ | Spearman | Pearson | Jaccard | Qualitative _) -> [||]
  | (SingleLink d | AverageLink d | CompleteLink d | Edit d | Warping (_,d) | Hausdorff d) -> [|d|]
  | (Average (_,ds) | Euclidean (_,ds) | Maximum (_,ds) | Median (_,ds) | Powermean (_,_,_,ds)) -> ds
