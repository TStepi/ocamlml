open Utils

type t = Data.t array -> Data.t

val calculate : Distance.c -> Statistic.t -> t
