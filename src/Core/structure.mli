open Utils

(** Posible data structures *)
type t =
  | Real
  | Integer
  | Binary
  | Nominal of string array
  | Tuple of t array
  | Set of t
  | Sequence of t
  | Series of int*t

(** Representation of data structure *)
val to_string : ?short:bool -> t -> string
