open Utils

type t = {
  description : string;
  to_string : unit -> string;
  predict : Data.t -> Data.t
}


let make_ensemble: string -> t array -> Aggregation.t -> t =
  fun description models aggregation ->
    {
      description;

      to_string = (fun () ->
          let acc = ref [] in
          Array.iteri models ~f:(fun i m ->
              acc := (sprintf "Member %d:\n%s\n\n" i (m.to_string ())) :: !acc
            );
          String.concat ~sep:"\n" (List.rev !acc)
        );

      predict = (fun d ->
          let predictions = Array.map models ~f:(fun m -> m.predict d) in
          aggregation predictions
        )
    }
