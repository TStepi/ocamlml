open Utils


type t = Data.t array -> Data.t


let voted possible_values neighbours =
  let votes = initialize_float_array possible_values in

  for i=0 to Array.length neighbours -1 do
    match neighbours.(i).Data.value with
    | None -> ()
    | Some v ->
      let value = int_of_float v in
      votes.(value) <- votes.(value) +. neighbours.(i).Data.reliability
  done;

  let best_ind = ref 0 in
  let best_w = ref votes.(0) in
  let total_w = ref 0. in
  for i=0 to Array.length votes -1 do
    total_w := !total_w +. votes.(i);
    if Float.compare votes.(!best_ind) votes.(i) = -1 then
      (best_ind := i; best_w := votes.(i));
  done;
  Data.nominal ~reliability:(!best_w /. !total_w) !best_ind


let weighted_mean starting_var rounded neighbours =
  let sum_w = ref 0. in
  let sum = ref 0. in
  let sumsq = ref 0. in
  for i=0 to Array.length neighbours - 1 do
    match neighbours.(i).Data.value with
    | None -> ()
    | Some v ->
      let w = neighbours.(i).Data.reliability in
      sum_w := !sum_w +. w;
      sum := !sum +. v*.w;
      sumsq := !sumsq +. v*.v*.w
  done;
  let mean = !sum /. !sum_w in
  let mean = if rounded then Float.round_nearest mean else mean in
  let var = !sumsq /. !sum_w -. (mean*.mean) in
  Data.real ~reliability:(1. -. var /. starting_var |> Float.max 0.) mean


let medoid starting_var dist neighbours =
  let best = ref Data.missing in
  let best_var = ref Float.infinity in
  for i=0 to Array.length neighbours -1 do
    let var = Statistic.l_variance dist neighbours neighbours.(i) in
    if var < !best_var then (best := neighbours.(i); best_var := var)
  done;
  {!best with Data.reliability = 1. -. !best_var /. starting_var |> Float.max 0.}


let rec calculate dist statistic =
  match statistic with
  | Statistic.Nominal freqs -> voted (Array.length freqs)
  | Statistic.Numeric stats -> weighted_mean (Statistic.variance stats) false
  | Statistic.Tuple (_,ss) -> fun tups ->
    let dist_comps = Distance.get_components dist in
    let data_comps = Array.map tups ~f:(fun t -> t.Data.components)
                     |> Array.transpose_exn in
    let centroid_comps = Array.mapi data_comps ~f:(fun i data ->
        calculate dist_comps.(i) ss.(i) data) in
    Data.tuple centroid_comps
  | _ -> failwith "Not yet implemented"

