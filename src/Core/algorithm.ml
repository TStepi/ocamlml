open Utils

module type Modeler =
sig
  val description : string
  val log : unit -> string
  val induce : Task.t -> (Model.t * time) array
end


module type Ranker =
sig
  val description : string
  val log : unit -> string
  val update_importance : Data.subset -> Data.subset
end


let ranked_modeler: (module Ranker) -> (module Modeler) -> (module Modeler) =
  fun ranker modeler ->
    (module struct
      module R = (val ranker : Ranker)
      module M = (val modeler : Modeler)

      let description = sprintf "%s with %s" M.description R.description

      let log () = sprintf "Ranker log:\n%s\n\nPredictor log:\n%s" (R.log ()) (M.log ())

      let induce task =
        let new_task = {task with Task.data = R.update_importance task.Task.data} in
        M.induce new_task
    end : Modeler)


let bagging_ensemble: (module Modeler) -> int -> (module Modeler) =
  fun modeler members ->
    (module struct
      module M = (val modeler : Modeler)

      let description = sprintf "Bagging of %s" M.description

      let log () = M.log ()

      let induce task =
        let start_time = time () in

        let trainset = task.Task.data in

        let aggregate = Aggregation.calculate
            task.Task.target_distance
            (Statistic.subset_target_stats trainset)
        in

        let models = List.init members ~f:(fun _ ->
            M.induce {task with Task.data = Data.bootstrap_subset trainset}
          ) |> Array.concat |> Array.map ~f:fst in

        let model = Model.make_ensemble description models aggregate in

        let stop_time = time () in
        [|model,stop_time -. start_time|]

    end : Modeler)


(* let optimize_parameters description parameters modeler_constructor = *)
(*   (module struct *)
(*     let decription = description *)

(*     let logger = ref [] *)
(*     let log () = String.concat ~sep:"\n" (List.rev !logger) *)

(*     let parameter_configurations = cartesian_product_multiple parameters *)

(*     let induce task = *)
(*       let c_num =  *)
(*       for c=0 to List.length paramete *)
(*   end : Modeler) *)
