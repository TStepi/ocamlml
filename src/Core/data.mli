open Utils

(**Type representing data.
   Value holds the value for primitive data, None means it is missing.
   Array components holds components of structured data and is empty for primitive. *)
type t = {
  value: float option;
  components: t array;
  reliability: float
}

(** Compares two data instances: values for primitive data, length of components for structured. *)
val compare : t -> t -> int

(**Returns a string representation of the data*)
val to_string : t -> string

(**Tests if given data is missing*)
val is_missing : t -> bool

(**Creates missing data*)
val missing : t

(**Creates nominal data*)
val nominal : ?reliability:float -> int -> t

(**Creates integer data*)
val integer : ?reliability:float -> int -> t

(**Creates real data*)
val real : ?reliability:float -> float -> t

(**Creates tuple data*)
val tuple : t array -> t

(**Creates set data*)
val set : t array -> t

(**Creates series data*)
val series : t array -> t

(**Creates sequence data*)
val sequence : t array -> t

(**Tests if given data has given structure*)
val has_structure : Structure.t -> t -> bool

(**Type representing a dataset - multiple attributes, multiple targets*)
type dataset = {
  name : string;
  structures : Structure.t array;
  values : t array array;
  variable_names : string array;
  size : int
}

(**A data subset with pointers to data in the dataset. Used to access the dataset. *)
type subset = {
  samples : int array;
  attributes : int array;
  importances : float array;
  targets : int array;
  dataset : dataset
}

(** Creates a subset containing all examples of a dataset and specified descriptive/target variables *)
val create_root_subset : dataset:dataset -> disabled:int array -> targets:int array -> subset

(**Returns the attributes of the samples in the subset *)
val get_attributes : subset -> t array
val get_attribute : subset -> int -> t

(**Returns the targets of the samples in the subset *)
val get_targets : subset -> t array
val get_target : subset -> int -> t

(** Returns the value of a given component of a given example*)
val get_datum : subset:subset -> example:int -> component:int -> t

val subset_complement : subset -> subset -> subset

val fold_subset : subset:subset -> folds:int -> fold:int -> subset

val bootstrap_subset : subset -> subset

val random_subset : subarray_size -> subset -> subset

val subspace_subset : subarray_size -> subset -> subset

val attribute_structure_of_subset : subset -> Structure.t

val target_structure_of_subset : subset -> Structure.t

val subset_to_arff : subset -> string -> unit
