open Utils

type numeric_stats = {
  mutable sum : float;
  mutable sumsq : float;
  mutable count : float;
}

type t =
  | Numeric of numeric_stats
  | Nominal of float array
  | Tuple of (float array)*(t array)
  | Set of t
  | Sequence of t
  | Series of int*t


let to_string ?(short=false) s =
  let acc = ref [] in
  let rec to_string n s =
    match s with
    | Nominal counts ->
        let descr = Array.to_list counts
                    |> List.to_string ~f:Float.to_string in
        acc := (sprintf "%sNominal %s" (pad n) descr) :: !acc
    | Numeric stats ->
      acc := (sprintf "%sReal (sum=%.3f  sumsq=%.3f  count=%.0f)"
                (pad n) stats.sum stats.sumsq stats.count) :: !acc
    | Set s ->
      acc := (sprintf "%sSet" (pad n)) :: !acc; to_string (n+4) s
    | Sequence s ->
      acc := (sprintf "%sSequence" (pad n)) :: !acc; to_string (n+4) s
    | Series (len,s) ->
      acc := (sprintf "%sSeries len=%d" (pad n) len) :: !acc; to_string (n+4) s
    | Tuple (ws,lst) ->
      if short then
        acc := (sprintf "%sTuple   dim = %d" (pad n) (Array.length lst)) :: !acc
      else begin
        acc := (sprintf "%sTuple\n" (pad n)) :: !acc;
        Array.iter ~f:(to_string (n+4)) lst
      end
  in
  to_string 0 s;
  let temp = List.rev !acc in
  String.concat ~sep:"\n" temp


let rec empty_copy stat =
  match stat with
  | Numeric _ -> Numeric {sum = 0.; sumsq = 0.; count = 0.}
  | Nominal counts -> Nominal (initialize_float_array (Array.length counts))
  | Tuple (ws,arr) -> Tuple (ws, Array.map arr ~f:empty_copy)
  | Set s -> Set (empty_copy s)
  | Sequence s -> Sequence (empty_copy s)
  | Series (n,s) -> Series (n,empty_copy s)


let rec copy stat =
  match stat with
  | Numeric s -> Numeric {sum = s.sum; sumsq = s.sumsq; count = s.count}
  | Nominal counts -> Nominal (Array.copy counts)
  | Tuple (ws,arr) -> Tuple (ws, Array.map arr ~f:copy)
  | Set s -> Set (copy s)
  | Sequence s -> Sequence (copy s)
  | Series (n,s) -> Series (n,copy s)



let rec calculate_stats structure data =
  match structure with
  | Structure.Nominal names ->
    let counts = initialize_float_array (Array.length names) in
    for i=0 to Array.length data -1 do
      match data.(i).Data.value with
      | None -> ()
      | Some v -> begin
          let v = Int.of_float v in
          counts.(v) <- counts.(v) +. 1.
        end
    done;
    Nominal counts

  | (Structure.Real | Structure.Integer | Structure.Binary) ->
    let count = ref 0 in
    let sum = ref 0. in
    let sumsq = ref 0. in
    for i=0 to Array.length data -1 do
      match data.(i).Data.value with
      | None -> ()
      | Some v -> begin
          incr count;
          sum := !sum +. v;
          sumsq := !sumsq +. v*.v;
        end
    done;
    Numeric {
      sum = !sum;
      sumsq = !sumsq;
      count = float !count
    }

  | Structure.Series (n,s) ->
    let unpacked = Array.map data ~f:(fun x -> x.Data.components)
                   |> Array.to_list
                   |> Array.concat
    in
    Series (n, calculate_stats s unpacked)

  | Structure.Tuple ss ->
    let component_data = Array.filter data ~f:(fun x -> not (Data.is_missing x))
                         |> Array.map ~f:(fun x -> x.Data.components)
                         |> Array.transpose_exn
    in
    Tuple (initialize_array (Array.length ss) 1.,
           Array.map2_exn ss component_data ~f:calculate_stats)

  | Structure.Set s ->
    let unpacked = Array.map data ~f:(fun x -> x.Data.components)
                   |> Array.to_list
                   |> Array.concat
    in
    Set (calculate_stats s unpacked)

  | Structure.Sequence s ->
    let unpacked = Array.map data ~f:(fun x -> x.Data.components)
                   |> Array.to_list
                   |> Array.concat
    in
    Sequence (calculate_stats s unpacked)



let subset_target_stats subset =
  Tuple (
    initialize_array (Array.length subset.Data.targets) 1.,
    Array.map subset.Data.targets ~f:(fun i ->
        calculate_stats
          subset.Data.dataset.Data.structures.(i)
          (Array.map subset.Data.samples ~f:(fun j -> subset.Data.dataset.Data.values.(j).(i)))
      )
  )


let subset_attribute_stats subset =
  Tuple (
    subset.Data.importances,
    Array.map subset.Data.attributes ~f:(fun i ->
        calculate_stats
          subset.Data.dataset.Data.structures.(i)
          (Array.map subset.Data.samples ~f:(fun j -> subset.Data.dataset.Data.values.(j).(i)))
      )
  )


let variance nstats =
  (nstats.sumsq -. nstats.sum *. nstats.sum /. nstats.count) /. nstats.count

let l_variance dist cluster center =
  let t = ref 0. in
  for i=0 to Array.length cluster -1 do
    let d = dist center cluster.(i) in
    t := !t +. cluster.(i).Data.reliability *. d *. d
  done;
  !t
