open Utils

type t = Statistic.t -> Distance.c
type pool = Statistic.t -> Distance.c array

type powermean_parameters =
{
  p : float;
  ow : (int -> int -> float);
  name : string;
}

type tuple_tag =
  | Powermean of powermean_parameters
  | Average
  | Euclidean
  | Median
  | Maximum

type sequence_tag =
  | Warping of int
  | Edit

type series_tag =
  | WarpingS of int
  | EditS
  | Qualitative
  | Pearson
  | Spearman
  | AverageS
  | EuclideanS
  | MedianS
  | MaximumS
  | PowermeanS of powermean_parameters

type set_tag =
  | Jaccard
  | Hausdorff
  | SingleLink
  | CompleteLink
  | AverageLink

val construct : tuple:tuple_tag ->
                seria: series_tag ->
                sequence:sequence_tag ->
                set:set_tag ->
                t

val construct_pool : tuples:tuple_tag array ->
                     series: series_tag array ->
                     sequences:sequence_tag array ->
                     sets:set_tag array ->
                     pool

val triangle : powermean_parameters
val average : powermean_parameters
val euclidean : powermean_parameters
val step : powermean_parameters
val sigmoid : powermean_parameters
val maximum : powermean_parameters
val median : powermean_parameters
val trimmed : powermean_parameters
val linear : powermean_parameters
val gauss : powermean_parameters
