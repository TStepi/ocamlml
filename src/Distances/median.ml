open Utils
  
let get_component_distances dims lst1 lst2 =
  Array.init (Array.length lst1) ~f:(fun i ->
    dims.(i) lst1.(i) lst2.(i)
  )

let calculate dws dims = fun d1 d2 -> 
  if Data.is_missing d1 || Data.is_missing d2 then 1.
  else begin
    let temp = get_component_distances dims d1.Data.components d2.Data.components
    |> Array.zip_exn dws in
    let n = Array.length temp in
    Array.sort temp ~cmp:(fun (w1,d1) (w2,d2) -> Float.ascending (w1*.d1) (w2*.d2));
    (snd temp.(n/2) +. snd temp.((n-1)/2)) *. 0.5
  end
  

let to_string indent short components dws =
  if short then
    (sprintf "%sMedian" (pad indent))
  else
    let lines =
      (sprintf "%sMedian" (pad indent)) ::
      (sprintf "%sdws=%s" (pad (indent+4)) (List.to_string ~f:(Float.to_string_hum ~decimals:3) (Array.to_list dws))) ::
      components
    in
    String.concat ~sep:"\n" lines