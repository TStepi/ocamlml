open Utils

let to_string indent = sprintf "%sDelta" (pad indent)

let calculate d1 d2 =
  if Data.is_missing d1 then 1.
  else if d1.Data.value = d2.Data.value then 0.
  else 1.