open Utils
    
let calculate dws weight_sum dims = fun d1 d2 -> 
  if Data.is_missing d1 || Data.is_missing d1 then 1.
  else begin
    let t = ref 0.0 in
    for i=0 to Array.length d1.Data.components -1 do
      t := !t +. dws.(i) *. (dims.(i) d1.Data.components.(i) d2.Data.components.(i))
    done;
    !t /. weight_sum
  end


let to_string indent short components dws =
  if short then
    (sprintf "%sAverage" (pad indent))
  else
    let lines =
      (sprintf "%sAverage" (pad indent)) ::
      (sprintf "%sdws=%s" (pad (indent+4)) (List.to_string ~f:(Float.to_string_hum ~decimals:3) (Array.to_list dws))) ::
      components
    in
    String.concat ~sep:"\n" lines
