open Utils

let to_string indent short comp =
  if short then
    sprintf "%sEdit distance" (pad indent)
  else
    sprintf "%sEdit distance\n%s" (pad indent) comp


let calculate d = fun d1 d2 ->
  if Data.is_missing d1 || Data.is_missing d2 then 1. else 
  (
    let m,n = Array.length d1.Data.components, Array.length d2.Data.components in
    if m = 0 then (if n=0 then 0. else 1.) else 
      let memo = Array.make_matrix 2 (m+1) Float.infinity in
      for j=0 to m do memo.(0).(j) <- float(j) done;
      for i=1 to n do
        memo.(i mod 2).(0) <- float(i);
        for j=1 to m do
          memo.(i mod 2).(j) <- min_3float
            (memo.((i-1) mod 2).(j-1) +. d d2.Data.components.(i-1) d1.Data.components.(j-1))
            (memo.((i-1) mod 2).(j) +.1.)
            (memo.(i mod 2).(j-1) +.1.)
        done
      done;
      memo.(n mod 2).(m) /. float(Int.max m n)
  )
