open Utils

let to_string indent =
  sprintf "%sPearson" (pad indent)

let coefficient xs ys =
  let n = Array.length xs in
  if n=0 then 0. else
  let xm = float_array_mean xs in
  let ym = float_array_mean ys in
  let numerator = ref 0. in
  let den1 = ref 0. in
  let den2 = ref 0. in
  for i=0 to n-1 do
    let dx = xs.(i) -. xm in
    let dy = ys.(i) -. ym in
    numerator := !numerator +. dx*.dy;
    den1 := !den1 +. dx*.dx;
    den2 := !den2 +. dy*.dy
  done;
  if !numerator = 0. then 0.
  else bounded_to ~lower:(-1.) ~upper:1. @@ !numerator /. ((!den1**0.5) *. (!den2**0.5))

let calculate d1 d2 =
  if Data.is_missing d1 || Data.is_missing d2 then 1.
  else begin
    if Array.exists ~f:Data.is_missing (Array.append d1.Data.components d2.Data.components) then 1. else
    let unpacked1 = Array.map d1.Data.components ~f:(fun x -> Option.value_exn x.Data.value) in
    let unpacked2 = Array.map d2.Data.components ~f:(fun x -> Option.value_exn x.Data.value) in
    sqrt (0.5*.(1. -. coefficient unpacked1 unpacked2))
  end
