open Utils

let calculate d1 d2 = 
  if Data.is_missing d1 || Data.is_missing d2 then 1.
  else begin
    Array.sort d1.Data.components ~cmp:Data.compare;
    Array.sort d2.Data.components ~cmp:Data.compare;
    let n1 = Array.length d1.Data.components in
    let n2 = Array.length d2.Data.components in
    let i = ref 0 in
    let j = ref 0 in
    let union = ref 0 in
    let inter = ref 0 in
    while (!i<n1 && !j<n2) do
      let c = Data.compare d1.Data.components.(!i) d2.Data.components.(!j) in
      if c = 0 then
        (incr i; incr j; incr union; incr inter)
      else if c < 0 then
        (incr i; incr union)
      else
        (incr j; incr union)
    done;
    1. -. (float !inter) /. (float (!union + n1 - !i + n2 - !j))
  end

let to_string indent = sprintf "%sJaccard" (pad indent)
