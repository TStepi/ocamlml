open Utils

let calculate b = fun d1 d2 ->
  if b = 0. then 0.
  else match d1.Data.value,d2.Data.value with
    | (None,_ | _,None) -> 1.
    | Some v1, Some v2 -> Float.min 1. (Float.abs (v1 -. v2) /. b)

let to_string indent short b = sprintf "%sRamp (%.3f)" (pad indent) b
