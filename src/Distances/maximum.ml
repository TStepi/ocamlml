open Utils

  let calculate dws dims = fun d1 d2 -> 
    if Data.is_missing d1 || Data.is_missing d2 then 1. 
    else begin
      let bi = ref 0 in 
      let bd = ref 0. in
      for i=1 to Array.length d1.Data.components -1 do
        let d = dims.(i) d1.Data.components.(i) d2.Data.components.(i) in
        if dws.(!bi) *. !bd < dws.(i) *. d then (bi := i; bd := d)
      done;
      !bd
    end
    

  let to_string indent short components dws =
    if short then
      (sprintf "%sMaximum" (pad indent))
    else
      let lines =
        (sprintf "%sMaximum" (pad indent)) ::
        (sprintf "%sdws=%s" (pad (indent+4)) (List.to_string ~f:(Float.to_string_hum ~decimals:3) (Array.to_list dws))) ::
        components
      in
      String.concat ~sep:"\n" lines
