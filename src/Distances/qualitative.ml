open Utils

let to_string indent = sprintf "%sQualitative" (pad indent)

let q tol a b =
  if Float.abs (a-.b) < tol then 0
  else Float.compare a b

let diff a b = Float.abs @@ float (a-b) /. 2.

let qual tol xs ys =
  let n = Array.length xs in
  if n=0 then 0. else
  let sum = ref 0. in
  for i=0 to n-2 do
    for j=i+1 to n-1 do
      sum := !sum +. (diff (q tol xs.(i) xs.(j)) (q tol ys.(i) ys.(j)))
    done
  done;
  2. *. !sum /. (float (n*(n-1)))

let calculate tol = fun d1 d2 ->
  if Data.is_missing d1 || Data.is_missing d2 then 1.
  else begin
    if Array.exists ~f:Data.is_missing (Array.append d1.Data.components d2.Data.components) then 1. else
    let unpacked1 = Array.map d1.Data.components ~f:(fun x -> Option.value_exn x.Data.value) in
    let unpacked2 = Array.map d2.Data.components ~f:(fun x -> Option.value_exn x.Data.value) in
    qual tol unpacked1 unpacked2
  end
