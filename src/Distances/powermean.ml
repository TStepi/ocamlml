open Utils

let get_component_distances dims lst1 lst2 =
  Array.init (Array.length lst1) ~f:(fun i ->
      dims.(i) lst1.(i) lst2.(i)
    )

let calculate p ows dws dims = fun d1 d2 ->
  if Data.is_missing d1 || Data.is_missing d2 then 1.
  else begin
    let ds = get_component_distances dims d1.Data.components d2.Data.components in
    let zipped = Array.zip_exn dws ds in
    Array.sort zipped ~cmp:(fun (a,b) (c,d) -> Float.compare (a*.b) (c*.d));
    let weights = Array.map2_exn ows zipped ~f:(fun o y -> o *. (fst y)) in
    float_array_make_convex weights;
    Array.fold2_exn weights zipped ~init:0. ~f:(fun s w d -> s +. w*.(snd d)** p)
    |> (fun x -> x**(1./. p))
  end

let to_string indent short components p ows dws =
  if short then
    (sprintf "%s%s" (pad indent) "Powermean")
  else
    let lines =
      (sprintf "%sPowerMean" (pad indent)) ::
      (sprintf "%sp=%.3f" (pad (indent+4)) p) ::
      (sprintf "%sows=%s" (pad (indent+4)) (List.to_string ~f:(Float.to_string_hum ~decimals:3) (Array.to_list ows))) ::
      (sprintf "%sdws=%s" (pad (indent+4)) (List.to_string ~f:(Float.to_string_hum ~decimals:3) (Array.to_list dws))) ::
      components
    in
    String.concat ~sep:"\n" lines
