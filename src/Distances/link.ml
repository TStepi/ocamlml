open Utils

let single d = fun d1 d2 ->
  if Data.is_missing d1 || Data.is_missing d2 then 1.
  else begin
    let dim1 = Array.length d1.Data.components in
    let dim2 = Array.length d2.Data.components in
    if dim1=0 then (if dim2=0 then 0. else 1.) else
    let t = ref 0. in
    for i=0 to dim1-1 do
      for j=0 to dim2-1 do
        t := Float.min !t (d d1.Data.components.(i) d2.Data.components.(j))
      done
    done;
    !t
  end

let single_string indent short component =
  if short then
    sprintf "%sSingle link" (pad indent)
  else
    sprintf "%sSingle link\n%s" (pad indent) component


let complete d = fun d1 d2 ->  
  if Data.is_missing d1 || Data.is_missing d2 then 1.
  else begin
    let dim1 = Array.length d1.Data.components in
    let dim2 = Array.length d2.Data.components in
    if dim1=0 then (if dim2=0 then 0. else 1.) else
    let t = ref 0. in
    for i=0 to dim1-1 do
      for j=0 to dim2-1 do
        t := Float.max !t (d d1.Data.components.(i) d2.Data.components.(j))
      done
    done;
    !t
  end


let complete_string indent short component =
  if short then
    sprintf "%sComplete link" (pad indent)
  else
    sprintf "%sComplete link\n%s" (pad indent) component


let average d = fun d1 d2 ->
  if Data.is_missing d1 || Data.is_missing d2 then 1.
  else begin
    let dim1 = Array.length d1.Data.components in
    let dim2 = Array.length d2.Data.components in
    if dim1=0 then (if dim2=0 then 0. else 1.) else
    let t = ref 0. in
    for i=0 to dim1-1 do
      for j=0 to dim2-1 do
        t := !t +. (d d1.Data.components.(i) d2.Data.components.(j))
      done
    done;
    !t /. (float (dim1*dim2))
  end

let average_string indent short component =
  if short then
    sprintf "%sAverage link" (pad indent)
  else
    sprintf "%sAverage link\n%s" (pad indent) component
