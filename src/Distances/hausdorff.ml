open Utils

  let calculate d = fun d1 d2 -> 
    if Data.is_missing d1 || Data.is_missing d2 then 1.
    else begin
      let dim1 = Array.length d1.Data.components in
      let dim2 = Array.length d2.Data.components in
      if dim1=0 && dim2=0 then 0. else
      if dim1=0 || dim2=0 then 1. else
      let row_mins = Array.create dim1 2. in
      let col_mins = Array.create dim2 2. in
      for i=0 to dim1-1 do
        for j=0 to dim2-1 do
          let dist = d d1.Data.components.(i) d2.Data.components.(j) in
          row_mins.(i) <- Float.min row_mins.(i) dist;
          col_mins.(j) <- Float.min col_mins.(j) dist
        done
      done;
      Float.max (float_array_max row_mins) (float_array_max col_mins)
    end

  let to_string indent short component =
    if short then
      sprintf "%sHausdorff" (pad indent)
    else
      sprintf "%sHausdorff\n%s" (pad indent) component
