open Utils

let to_string window indent short component =
  if short then
    sprintf "%sTime Warping w=%d" (pad indent) window
  else
    sprintf "%sTime Warping w=%d\n%s" (pad indent) window component

let calculate window d = fun d1 d2 ->
  if Data.is_missing d1 || Data.is_missing d2 then 1.
  else begin
    let m,n = Array.length d1.Data.components, Array.length d2.Data.components in
    if m = 0 then (if n=0 then 0. else 1.) 
    else
      let w = Int.max (Int.abs (m-n)) ((m+n)*window / 100) in
      let memo = Array.make_matrix 2 (n+1) Float.infinity in
      memo.(0).(0) <- 0.;
      for i=1 to m do
        for j=(Int.max 1 (i-w)) to (Int.min n (i+w)) do
          memo.(i mod 2).(j) <- d d1.Data.components.(i-1) d2.Data.components.(j-1) +. min_3float
            memo.((i-1) mod 2).(j) memo.(i mod 2).(j-1) memo.((i-1) mod 2).(j-1)
        done
      done;
      memo.(m mod 2).(n) /. (float @@ Int.max m n)
  end